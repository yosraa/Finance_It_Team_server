package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class AchatCurrencyOption implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private AchatCurrencyOptionPk achatCurrencyOptionPK;
	@Temporal(TemporalType.DATE)
	private Date settelmentDate;

	@Enumerated(EnumType.STRING)
	private StateTrade stateTrade;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCurrencyOption", referencedColumnName = "id", insertable = false, updatable = false)
	private CurrencyOption currencyOption;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTrader", referencedColumnName = "id", insertable = false, updatable = false)
	private Trader trader;

	public AchatCurrencyOptionPk getAchatCurrencyOptionPK() {
		return achatCurrencyOptionPK;
	}

	public void setAchatCurrencyOptionPK(AchatCurrencyOptionPk achatCurrencyOptionPK) {
		this.achatCurrencyOptionPK = achatCurrencyOptionPK;
	}

	public Date getSettelmentDate() {
		return settelmentDate;
	}

	public void setSettelmentDate(Date settelmentDate) {
		this.settelmentDate = settelmentDate;
	}

	public StateTrade getStateTrade() {
		return stateTrade;
	}

	public void setStateTrade(StateTrade stateTrade) {
		this.stateTrade = stateTrade;
	}

	public CurrencyOption getCurrencyOption() {
		return currencyOption;
	}

	public void setCurrencyOption(CurrencyOption currencyOption) {
		this.currencyOption = currencyOption;
	}

	public Trader getTrader() {
		return trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	@Override
	public String toString() {
		return "AchatCurrencyOption [achatCurrencyOptionPK=" + achatCurrencyOptionPK + ", settelmentDate="
				+ settelmentDate + ", stateTrade=" + stateTrade + ", currencyOption=" + currencyOption + ", trader="
				+ trader + ", getAchatCurrencyOptionPK()=" + getAchatCurrencyOptionPK() + ", getSettelmentDate()="
				+ getSettelmentDate() + ", getStateTrade()=" + getStateTrade() + ", getCurrencyOption()="
				+ getCurrencyOption() + ", getTrader()=" + getTrader() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}