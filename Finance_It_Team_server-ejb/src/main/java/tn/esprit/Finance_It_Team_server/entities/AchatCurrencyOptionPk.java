package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class AchatCurrencyOptionPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idTrader;
	private int idCurrencyOption;
	private String nom;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCurrencyOption;
		result = prime * result + idTrader;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AchatCurrencyOptionPk other = (AchatCurrencyOptionPk) obj;
		if (idCurrencyOption != other.idCurrencyOption)
			return false;
		if (idTrader != other.idTrader)
			return false;
		return true;
	}

	public int getIdTrader() {
		return idTrader;
	}

	public void setIdTrader(int idTrader) {
		this.idTrader = idTrader;
	}

	public int getIdCurrencyOption() {
		return idCurrencyOption;
	}

	public void setIdCurrencyOption(int idCurrencyOption) {
		this.idCurrencyOption = idCurrencyOption;
	}

}
