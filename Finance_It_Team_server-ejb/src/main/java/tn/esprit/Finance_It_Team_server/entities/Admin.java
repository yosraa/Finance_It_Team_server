package tn.esprit.Finance_It_Team_server.entities;
import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Admin extends User implements Serializable {

		public Admin(){
			super();
		}
		public Admin(String userName, String firstName, String lastName, String email, String password, String gender,
				int phoneNumber) {
			super(userName, firstName, lastName, email, password, gender, phoneNumber);
	}
}
