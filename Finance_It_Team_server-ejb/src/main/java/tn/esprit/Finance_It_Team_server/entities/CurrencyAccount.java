package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.TypedQuery;

@Entity
public class CurrencyAccount implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
private int id;
private float amount;
private float sumIncome;
private float sumExpenses;
@Enumerated(EnumType.STRING)
private TypeCurrency typecurrency;
public CurrencyAccount() {
	}


public CurrencyAccount(float amount, float sumIncome, float sumExpenses, TypeCurrency typecurrency) {
	
	this.amount = amount;
	this.sumIncome = sumIncome;
	this.sumExpenses = sumExpenses;
	this.typecurrency = typecurrency;
	//this.trader = trader;
}


public TypeCurrency getTypecurrency() {
	return typecurrency;
}
public void setTypecurrency(TypeCurrency typecurrency) {
	this.typecurrency = typecurrency;
}
@ManyToOne
private Trader trader;



public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public float getAmount() {
	return amount;
}


public void setAmount(float amount) {
	this.amount = amount;
}


public float getSumIncome() {
	return sumIncome;
}


public void setSumIncome(float sumIncome) {
	this.sumIncome = sumIncome;
}


public float getSumExpenses() {
	return sumExpenses;
}


public void setSumExpenses(float sumExpenses) {
	this.sumExpenses = sumExpenses;
}


public Trader getTrader() {
	return trader;
}


public void setTrader(Trader trader) {
	this.trader = trader;
}


@Override
public String toString() {
	return "CurrencyAccount [id=" + id + ", amount=" + amount + ", sumIncome=" + sumIncome + ", sumExpenses="
			+ sumExpenses + ", typecurrency=" + typecurrency + "]";
}


public CurrencyAccount(int id, float amount, float sumIncome, float sumExpenses, TypeCurrency typecurrency) {
	super();
	this.id = id;
	this.amount = amount;
	this.sumIncome = sumIncome;
	this.sumExpenses = sumExpenses;
	this.typecurrency = typecurrency;
	
}




}
