package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class CurrencyOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private float strikePrice;
	private float currentPrice;
	@Enumerated(EnumType.STRING)
	private TypeOption typeoption;
	@Temporal(TemporalType.DATE)
	private Date expirationDate;
	private float volatility;
	private float risk;
	private float time;
	private float spotExchangeRate;
	@Enumerated(EnumType.STRING)
	private UnitExchange unitEchange; // (from currency to currency)
	@Enumerated(EnumType.STRING)
	private StateOption stateOption;
	private double premium;
	private int Archive;

	public int getArchive() {
		return Archive;
	}

	public void setArchive(int archive) {
		Archive = archive;
	}

	// @ManyToMany(cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	@ManyToOne( fetch = FetchType.EAGER )
	private Trader trader;
	@OneToMany(mappedBy = "currencyOption",cascade={CascadeType.REMOVE})
	private List<AchatCurrencyOption> achats;

	public List<AchatCurrencyOption> getAchats() {
		return achats;
	}

	public void setAchats(List<AchatCurrencyOption> achats) {
		this.achats = achats;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getStrikePrice() {
		return strikePrice;
	}

	public void setStrikePrice(float strikePrice) {
		this.strikePrice = strikePrice;
	}

	public float getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(float currentPrice) {
		this.currentPrice = currentPrice;
	}

	public TypeOption getTypeoption() {
		return typeoption;
	}

	public void setTypeoption(TypeOption typeoption) {
		this.typeoption = typeoption;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public float getVolatility() {
		return volatility;
	}

	public void setVolatility(float volatility) {
		this.volatility = volatility;
	}

	public float getRisk() {
		return risk;
	}

	public void setRisk(float risk) {
		this.risk = risk;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public StateOption getStateOption() {
		return stateOption;
	}

	public void setStateOption(StateOption stateOption) {
		this.stateOption = stateOption;
	}

	public UnitExchange getUnitEchange() {
		return unitEchange;
	}

	public void setUnitEchange(UnitExchange unitEchange) {
		this.unitEchange = unitEchange;
	}

	public float getSpotExchangeRate() {
		return spotExchangeRate;
	}

	public void setSpotExchangeRate(float spotExchangeRate) {
		this.spotExchangeRate = spotExchangeRate;
	}

	public CurrencyOption(float currentPrice, float strikePrice, float risk, float volatility, float time,
			TypeOption typeoption, Date expirationDate, UnitExchange unitEchange, float spotExchangeRate,double premium) {

		this.strikePrice = strikePrice;
		this.currentPrice = currentPrice;
		this.typeoption = typeoption;
		this.volatility = volatility;
		this.risk = risk;
		this.time = time;
		this.expirationDate = expirationDate;
		this.unitEchange = unitEchange;
		this.spotExchangeRate = spotExchangeRate;
		this.premium = premium;
		
	}
	
	public CurrencyOption(int id) {

		this.id = id;
		
	}

	public CurrencyOption() {

	}

	


	@Override
	public String toString() {
		return "CurrencyOption [id=" + id + ", strikePrice=" + strikePrice + ", currentPrice=" + currentPrice
				+ ", typeoption=" + typeoption + ", expirationDate=" + expirationDate + ", volatility=" + volatility
				+ ", risk=" + risk + ", time=" + time + ", spotExchangeRate=" + spotExchangeRate + ", unitEchange="
				+ unitEchange + ", stateOption=" + stateOption + ", premium=" + premium + "]";
	}

	public Trader getTrader() {
		return trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}
	
}
