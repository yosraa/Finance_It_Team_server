package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Reclamation implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String description;
	private static final long serialVersionUID = 1L;
	private int traderId;
	
	public Reclamation(int id, String title, String description, int traderId, String state) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.traderId = traderId;
		this.state = state;
	}
	private String state;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public Reclamation() {
		super();
	}
	public int getTraderId() {
		return traderId;
	}
	public void setTraderId(int traderId) {
		this.traderId = traderId;
	}

}
