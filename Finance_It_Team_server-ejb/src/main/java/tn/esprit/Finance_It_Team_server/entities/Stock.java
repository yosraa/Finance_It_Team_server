package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Stock implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private float nbStockTotal; // 9adeh fama men share fel option hedhi exp: 10 puissance 6 shares
	private float valueTotal; //la valeur totale de l'option exp: one billion $ 
	
	@OneToMany(mappedBy="stock") //malek (association between StockOption and Stock)
	
	
	private List<StockOption> stockoption;
	
	
	public Stock() {}
	
	public Stock( float nbStockTotal, float valueTotal) {
		super();
		this.nbStockTotal = nbStockTotal;
		this.valueTotal = valueTotal;
	}
	
	public Stock(int id, float nbStockTotal, float valueTotal) {
		super();
		this.id = id;
		this.nbStockTotal = nbStockTotal;
		this.valueTotal = valueTotal;
	}
	
	
public Stock(int id, float nbStockTotal, float valueTotal, List<StockOption> stockoption) {
		super();
		this.id = id;
		this.nbStockTotal = nbStockTotal;
		this.valueTotal = valueTotal;
		this.stockoption = stockoption;
	}

	


	
	public List<StockOption> getStockoption() {
	return stockoption;
}

public void setStockoption(List<StockOption> stockoption) {
	this.stockoption = stockoption;
}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getNbStockTotal() {
		return nbStockTotal;
	}
	public void setNbStockTotal(float nbStockTotal) {
		this.nbStockTotal = nbStockTotal;
	}
	public float getValueTotal() {
		return valueTotal;
	}
	public void setValueTotal(float valueTotal) {
		this.valueTotal = valueTotal;
	}


	
	
	
}
