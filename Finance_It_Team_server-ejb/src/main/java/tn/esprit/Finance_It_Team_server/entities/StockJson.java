package tn.esprit.Finance_It_Team_server.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.github.kevinsawicki.http.HttpRequest;

import org.json.JSONArray;
import org.json.JSONObject;

public class StockJson {

	public List<Stocks> showHistoriqueDataBySymbole(String symbole) {

		String response = HttpRequest.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="
				+ symbole + "&interval=1min&apikey=02T4VFNIZQ1QHZ0H").accept("application/json").body();

		// https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+
		// symbole+"&interval=1min&apikey=02T4VFNIZQ1QHZ0H"

		/*
		 * String response = HttpRequest .get(
		 * "https://finance.yahoo.com/quote/AMZN?ltr=1&type=daily&startDate=20180100000000&guccounter=1")
		 * .accept("application/json").body();
		 */
		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("status");
		String strCode = status.getDouble("code") + "";
		if (!strCode.equals("200.0"))
			return Collections.emptyList();
		//
		JSONArray result = jsonObject.getJSONArray("results");
		List<Stocks> lst = new ArrayList<>();
		for (int i = 0; i < result.length(); i++) {
			JSONObject data = result.getJSONObject(i);
			Stocks stocks = new Stocks();
			stocks.setRef(i + "");
			stocks.setClose(data.getDouble("close") + "");
			stocks.setVolume(data.getInt("volume") + "");
			lst.add(stocks);
		}
		return lst;
	}

	public List<Stocks> showFutureData(String symbols) {

		/*
		 * String response = HttpRequest .get(
		 * "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+
		 * symbols +"&interval=1min&apikey=02T4VFNIZQ1QHZ0H")
		 * .accept("application/json").body();
		 */

		String response = HttpRequest.get("https://marketdata.websol.barchart.com"
				+ "/getQuote.json?apikey=5762666332e2d8a7b5374845ed4c5513&symbols=AMZN"
				+ "ZC*1,IBM,GOOGL,%5EEURUSD,AMZN,AAPL,FB,NFLX,INTC,"
				+ "TGT,BAC,PIH,TSLA,BABA,F,MU,INTC,GE,TWTR,WMT,NOK,"
				+ "QQQ,WDC,ECYT,ADMP,GM,TI,SRPT,LRCX,TRXC,TSRO,CSPI," + "CASA,BB,GILD,XOM,PBR,AXP,QCOM,FIT,UAA,DAL,CVS")
				.accept("application/json").body();

		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("status");
		String strCode = status.getDouble("code") + "";
		if (!strCode.equals("200.0"))
			Collections.emptyList();
		//
		JSONArray result = jsonObject.getJSONArray("results");
		List<Stocks> lst = new ArrayList<>();
		for (int i = 0; i < result.length(); i++) {
			JSONObject data = result.getJSONObject(i);
			Stocks stocks = new Stocks();
			stocks.setRef(i + "");
			stocks.setClose(data.getDouble("close") + "");
			stocks.setHigh(data.getDouble("high") + "");
			stocks.setLow(data.getDouble("low") + "");
			stocks.setName(data.getString("name"));
			stocks.setOpen(data.getDouble("open") + "");
			stocks.setSymbol(data.getString("symbol"));

			stocks.setVolume(data.getInt("volume") + "");
			stocks.setName(data.getString("name"));
			stocks.setGainToBuy(Double.parseDouble(stocks.getHigh())*Double.parseDouble(stocks.getOpen()));

			lst.add(stocks);
		}
		return lst;
	}

}
