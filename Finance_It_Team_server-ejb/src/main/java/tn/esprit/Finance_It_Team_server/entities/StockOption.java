package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class StockOption implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;

	private float strikePriceStock;
	private float currentPriceStock;
	@Enumerated(EnumType.STRING)
	private TypeOptionStock typeOptionStock;  //enum hedhi lezemha classe ennum  
	@Temporal(TemporalType.TIMESTAMP)   //system date
	private Date expirationDateStock;    // nzid nchouf le type mta3 l date
	private float volatilityStock;
	private float riskStock;
	private float timeStock;
	@Enumerated(EnumType.STRING)
	private StateOptionStock stateOptionStock; //enum hedhi lezemha classe ennum
	private Date dateOptionBegin;
	private Date dateOptionEnd;
	private float nbShares;  // nb mta3 les shares eli bech yetchrew %
	@ManyToOne
	private Trader trader;
	@ManyToOne
	private Stock stock;
	
	
	public StockOption() {}
		
	public StockOption(float strikePriceStock, float currentPriceStock, TypeOptionStock typeOptionStock,
			Date expirationDateStock, float volatilityStock, float riskStock, float timeStock,
			StateOptionStock stateOptionStock, Date dateOptionBegin, Date dateOptionEnd, float nbShares) {
		super();
		this.strikePriceStock = strikePriceStock;
		this.currentPriceStock = currentPriceStock;
		this.typeOptionStock = typeOptionStock;
		this.expirationDateStock = expirationDateStock;
		this.volatilityStock = volatilityStock;
		this.riskStock = riskStock;
		this.timeStock = timeStock;
		this.stateOptionStock = stateOptionStock;
		this.dateOptionBegin = dateOptionBegin;
		this.dateOptionEnd = dateOptionEnd;
		this.nbShares = nbShares;
		
	}



	public StockOption(int id, float strikePriceStock, float currentPriceStock, TypeOptionStock typeOptionStock,
			Date expirationDateStock, float volatilityStock, float riskStock, float timeStock,
			StateOptionStock stateOptionStock, Date dateOptionBegin, Date dateOptionEnd, float nbShares) {
		super();
		this.id = id;
		this.strikePriceStock = strikePriceStock;
		this.currentPriceStock = currentPriceStock;
		this.typeOptionStock = typeOptionStock;
		this.expirationDateStock = expirationDateStock;
		this.volatilityStock = volatilityStock;
		this.riskStock = riskStock;
		this.timeStock = timeStock;
		this.stateOptionStock = stateOptionStock;
		this.dateOptionBegin = dateOptionBegin;
		this.dateOptionEnd = dateOptionEnd;
		this.nbShares = nbShares;
	}

	public StockOption(int id, float strikePriceStock, float currentPriceStock, TypeOptionStock typeOptionStock,
			Date expirationDateStock, float volatilityStock, float riskStock, float timeStock,
			StateOptionStock stateOptionStock, Date dateOptionBegin, Date dateOptionEnd, float nbShares,
			Trader trader) {
		super();
		this.id = id;
		this.strikePriceStock = strikePriceStock;
		this.currentPriceStock = currentPriceStock;
		this.typeOptionStock = typeOptionStock;
		this.expirationDateStock = expirationDateStock;
		this.volatilityStock = volatilityStock;
		this.riskStock = riskStock;
		this.timeStock = timeStock;
		this.stateOptionStock = stateOptionStock;
		this.dateOptionBegin = dateOptionBegin;
		this.dateOptionEnd = dateOptionEnd;
		this.nbShares = nbShares;
		this.trader = trader;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getStrikePriceStock() {
		return strikePriceStock;
	}
	public void setStrikePriceStock(float strikePriceStock) {
		this.strikePriceStock = strikePriceStock;
	}
	public float getCurrentPriceStock() {
		return currentPriceStock;
	}
	public void setCurrentPriceStock(float currentPriceStock) {
		this.currentPriceStock = currentPriceStock;
	}
	public TypeOptionStock getTypeOptionStock() {
		return typeOptionStock;
	}
	public void setTypeOptionStock(TypeOptionStock typeOptionStock) {
		this.typeOptionStock = typeOptionStock;
	}
	public Date getExpirationDateStock() {
		return expirationDateStock;
	}
	public void setExpirationDateStock(Date expirationDateStock) {
		this.expirationDateStock = expirationDateStock;
	}
	public float getVolatilityStock() {
		return volatilityStock;
	}
	public void setVolatilityStock(float volatilityStock) {
		this.volatilityStock = volatilityStock;
	}
	public float getRiskStock() {
		return riskStock;
	}
	public void setRiskStock(float riskStock) {
		this.riskStock = riskStock;
	}
	public float getTimeStock() {
		return timeStock;
	}
	public void setTimeStock(float timeStock) {
		this.timeStock = timeStock;
	}
	public StateOptionStock getStateOptionStock() {
		return stateOptionStock;
	}
	public void setStateOptionStock(StateOptionStock stateOptionStock) {
		this.stateOptionStock = stateOptionStock;
	}
	public Date getDateOptionBegin() {
		return dateOptionBegin;
	}
	public void setDateOptionBegin(Date dateOptionBegin) {
		this.dateOptionBegin = dateOptionBegin;
	}
	public Date getDateOptionEnd() {
		return dateOptionEnd;
	}
	public void setDateOptionEnd(Date dateOptionEnd) {
		this.dateOptionEnd = dateOptionEnd;
	}
	public float getNbShares() {
		return nbShares;
	}
	public void setNbShares(float nbShares) {
		this.nbShares = nbShares;
	}
	public Trader getTrader() {
		return trader;
	}
	public void setTrader(Trader trader) {
		this.trader = trader;
	}
	
	

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "StockOption [id=" + id + ", strikePriceStock=" + strikePriceStock + ", currentPriceStock="
				+ currentPriceStock + ", typeOptionStock=" + typeOptionStock + ", expirationDateStock="
				+ expirationDateStock + ", volatilityStock=" + volatilityStock + ", riskStock=" + riskStock
				+ ", timeStock=" + timeStock + ", stateOptionStock=" + stateOptionStock + ", dateOptionBegin="
				+ dateOptionBegin + ", dateOptionEnd=" + dateOptionEnd + ", nbShares=" + nbShares + ", trader=" + trader
				+ ", stock=" + stock + "]";
	}




	

	
	
	
	
}
