package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;



import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Stocks {
	private String Ref;
	private String symbol;
	private String name;
	private String open;
	private String high;
	private String low;
	private String close;
	private String volume;
	private Double gainToBuy;
	private Double gainToSell;
	private Double ohlc;
	private Double dsa;

	public String getRef() {
		return Ref;
	}
	public void setRef(String ref) {
		Ref = ref;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOpen() {
		return open;
	}
	public void setOpen(String open) {
		this.open = open;
	}
	public String getHigh() {
		return high;
	}
	public void setHigh(String high) {
		this.high = high;
	}
	public String getLow() {
		return low;
	}
	public void setLow(String low) {
		this.low = low;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	@Override
	public String toString() {
		return "Stocks [Ref=" + Ref + ", symbol=" + symbol + ", name=" + name + ", open=" + open + ", high=" + high
				+ ", low=" + low + ", close=" + close + ", volume=" + volume + "]";
	}
	public Double getGainToBuy() {
		return gainToBuy;
	}
	public void setGainToBuy(Double gainToBuy) {
		this.gainToBuy = gainToBuy;
	}
	public Double getGainToSell() {
		return gainToSell;
	}
	public void setGainToSell(Double gainToSell) {
		this.gainToSell = gainToSell;
	}
	public Double getOhlc() {
		return ohlc;
	}
	public void setOhlc(Double ohlc) {
		this.ohlc = ohlc;
	}
	public Double getDsa() {
		return dsa;
	}
	public void setDsa(Double dsa) {
		this.dsa = dsa;
	}

	
}

