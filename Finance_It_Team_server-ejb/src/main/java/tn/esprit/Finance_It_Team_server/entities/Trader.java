package tn.esprit.Finance_It_Team_server.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
//@DiscriminatorValue(value="typeTrader")
public class Trader extends User implements Serializable {
	
	public Trader() {
		
	}
	public Trader(String firstName, String lastName, String email, String password, String gender,
			int phoneNumber,String scoring, String statusTrader) {
		super(firstName,lastName,email,password,gender,phoneNumber);
		this.scoring = scoring;
		this.statusTrader = statusTrader;

	}
	public Trader(String userName,String firstName, String lastName, String email, String password, String gender,
			int phoneNumber,String scoring, String statusTrader) {
		super(userName,firstName,lastName,email,password,gender,phoneNumber);
		this.scoring = scoring;
		this.statusTrader = statusTrader;

	}
	
	/*@OneToOne(mappedBy = "trader")
	private Portfolio portfolio;*/
	
	

		

	private String scoring ;
private String statusTrader;

@OneToMany(mappedBy="trader")
private List<CurrencyOption> optionProduct;

@OneToMany(mappedBy="trader")
private List<CurrencyAccount> currencyAccounts;

//-----------------------------------------MALEK-------------------------------
@OneToOne(mappedBy = "trader")//malek (association between Portfolio and Trader)
private Portfolio portfolio;
@OneToMany(mappedBy="trader") //malek (association between StockOption and Trader)
private List<StockOption> stockoption;

public Portfolio getPortfolio() { //malek
	return portfolio;
}
public void setPortfolio(Portfolio portfolio) { //malek
	this.portfolio = portfolio;
}
public List<StockOption> getStockoption() {   //malek
	return stockoption;
}
public void setStockoption(List<StockOption> stockoption) {   //malek
	this.stockoption = stockoption;
}
//--------------------------------------------------------------------------------



public String getScoring() {
	return scoring;
}
public void setScoring(String scoring) {
	this.scoring = scoring;
}

public List<CurrencyAccount> getCurrencyAccounts() {
	return currencyAccounts;
}
public void setCurrencyAccounts(List<CurrencyAccount> currencyAccounts) {
	this.currencyAccounts = currencyAccounts;
}
public String getStatusTrader() {
	return statusTrader;
}
public void setStatusTrader(String statusTrader) {
	this.statusTrader = statusTrader;
}
public List<CurrencyOption> getOptionProduct() {
	return optionProduct;
}
public void setOptionProduct(List<CurrencyOption> optionProduct) {
	this.optionProduct = optionProduct;
}
}
