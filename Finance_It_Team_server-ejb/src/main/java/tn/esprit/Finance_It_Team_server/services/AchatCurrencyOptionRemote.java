package tn.esprit.Finance_It_Team_server.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOption;



@Remote
public interface AchatCurrencyOptionRemote {
	public int addOptionProduct(CurrencyOption optionp);

	public int addOptionUser(Trader t);

	public void affecterTraderOption(int TraderId, int CurrencyOptionId);

	public void ajouterAchatCurrencyOption(int idTrader, int idCurrencyOption, String nom, Date settelmentDate,StateTrade stateTrade);
   
	public List<AchatCurrencyOption> getAllTrade();

	public float getAmountFromCurrencyAccount11();

	public float getAmountFromCurrencyAccount12();
	
	public void setAmount(Trader t , float am);

	public float getAmountFromCurrencyAccount();
	
	public Trader findtrader(int id);

	public List<AchatCurrencyOption> getAllTradePending();

	public float getStrikePrice(int id);
}

