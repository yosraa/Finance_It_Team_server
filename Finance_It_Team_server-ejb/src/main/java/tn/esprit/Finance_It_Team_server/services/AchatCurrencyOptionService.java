package tn.esprit.Finance_It_Team_server.services;


import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOptionPk;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.TypeCurrency;


@Stateless
@LocalBean
public class AchatCurrencyOptionService implements AchatCurrencyOptionRemote {
	@PersistenceContext(unitName = "Finance_It_Team_server-ejb")
	EntityManager em;

	@Override
	public int addOptionProduct(CurrencyOption optionp) {
		em.persist(optionp);
		return optionp.getId();
	}

	@Override
	public int addOptionUser(Trader t) {
		em.persist(t);
		return t.getId();
	}

	@Override
	public void affecterTraderOption(int TraderId, int CurrencyOptionId) {
		Trader t = em.find(Trader.class, TraderId);
		CurrencyOption c = em.find(CurrencyOption.class, CurrencyOptionId);
		c.setTrader(t);
	}

	@Override
	public void ajouterAchatCurrencyOption(int idTrader, int idCurrencyOption, String nom, Date settelmentDate,StateTrade stateTrade) {
		AchatCurrencyOptionPk achatCurrencyOptionPk = new AchatCurrencyOptionPk();
		achatCurrencyOptionPk.setIdTrader(idTrader);
		achatCurrencyOptionPk.setIdCurrencyOption(idCurrencyOption);
		achatCurrencyOptionPk.setNom(nom);
		AchatCurrencyOption achatCurrencyOption = new AchatCurrencyOption();
		achatCurrencyOption.setSettelmentDate(settelmentDate);
		
	    achatCurrencyOption.setStateTrade(stateTrade);
		achatCurrencyOption.setAchatCurrencyOptionPK(achatCurrencyOptionPk);
		em.persist(achatCurrencyOption);

	}
	@Override
	public List<AchatCurrencyOption> getAllTrade() {
		TypedQuery<AchatCurrencyOption> query = em.createQuery("Select a from AchatCurrencyOption a", AchatCurrencyOption.class);
		return query.getResultList();
	}
	@Override
	public List<AchatCurrencyOption> getAllTradePending() {
		TypedQuery<AchatCurrencyOption> query = em.createQuery("Select a from AchatCurrencyOption a where StateTrade like '%pending%'", AchatCurrencyOption.class);
		return query.getResultList();
	}
	@Override
	public float getAmountFromCurrencyAccount() {
		javax.persistence.Query query = em
				.createQuery("Select c.amount from CurrencyAccount c WHERE c.typecurrency =:param");
		query.setParameter("param",TypeCurrency.euro);
		float nombre = (float) query.getSingleResult();
		return nombre;

		
	}

@Override
public float getAmountFromCurrencyAccount11() {
	javax.persistence.Query query = em
			.createQuery("Select c.amount from CurrencyAccount c WHERE c.typecurrency =:param");
	query.setParameter("param",TypeCurrency.yen);
	float nombre = (float) query.getSingleResult();
	return nombre;

	
}

public float getAmountFromCurrencyAccount12() {
	javax.persistence.Query query = em
			.createQuery("Select c.amount from CurrencyAccount c WHERE c.typecurrency =:param");
	query.setParameter("param",TypeCurrency.pound);
	float nombre = (float) query.getSingleResult();
	return nombre;

	
}
public float getStrikePrice(int id) {
	javax.persistence.Query query = em
			.createQuery("Select c.strikePrice from CurrencyOption c WHERE c.id =:id");
	query.setParameter("param",id);
	float nombre = (float) query.getSingleResult();
	return nombre;

	
}
public float getStrikePrice1() {
	javax.persistence.Query query = em
			.createQuery("Select c.strikePrice from CurrencyOption c WHERE c.id =:id");
	
	float nombre = (float) query.getSingleResult();
	return nombre;

	
}
public void setAmount(Trader t , float am)
{
	javax.persistence.Query query = em
			.createQuery("update CurrencyAccount c set c.amount=:param where c.trader=:t");
	
	        query.setParameter("t",t);
	        query.setParameter("param", am);
	        query.executeUpdate();
	}
public Trader findtrader(int id){
	Trader trader =em.find(Trader.class,id);
	return trader;
}
public long calculerPending() {
	javax.persistence.Query query = em
			.createQuery("SELECT COUNT(c.stateTrade) FROM AchatCurrencyOption c WHERE c.stateTrade like '%Pending%'");
	
	long nombre = (long) query.getSingleResult();
	return nombre;

}


}
