package tn.esprit.Finance_It_Team_server.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;

import tn.esprit.Finance_It_Team_server.utils.Exchange;

import static java.lang.Math.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/*************** Méthode de Black & Scholes *************************/

@Stateless
@LocalBean
public class CalculService implements CalculServiceRemote {
	public double callPrice(double s, double x, double r, double sigma, double t) {
		double d1 = (Math.log(s / x) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));
		double d2 = d1 - sigma * Math.sqrt(t);
		return s * cdf(d1) - x * Math.exp(-r * t) * cdf(d2);
	}

	public double putPrice(double s, double x, double r, double sigma, double t) {
		double d1 = (Math.log(s / x) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));
		double d2 = d1 - sigma * Math.sqrt(t);
		return -s * cdf(-d1) + x * Math.exp(-r * t) * cdf(-d2);
	}

	public double pdf(double x) {
		return Math.exp(-x * x / 2) / Math.sqrt(2 * Math.PI);
	}

	public double pdf(double x, double mu, double sigma) {
		return pdf((x - mu) / sigma) / sigma;

	}

	public double cdf(double z) {
		if (z < -8.0)
			return 0.0;
		if (z > 8.0)
			return 1.0;
		double sum = 0.0;
		double term = z;
		for (int i = 3; sum + term != sum; i += 2) {
			sum = sum + term;
			term = term * z * z / i;
		}
		return 0.5 + sum * pdf(z);
	}

	public double cdf(double z, double mu, double sigma) {
		return cdf((z - mu) / sigma);
	}

	/*******************************************************/

	/************* Parity Pricing Model *****************/
	public double Parity(double T, double S, double K, double r, double sigma, double q, String Type) {
		if (Type.equalsIgnoreCase("call")) {
			double P = putPrice(S, K, r, sigma, T);
			return P + S - K;

		} else if (Type.equalsIgnoreCase("put")) {
			double C = callPrice(S, K, r, sigma, T);
			return C + K - S;
		} else
			return 0;

	}

	/************* Binomial model *****************/
	public double putOptionValue(double _spotPrice, double strickPrice, double yearsToExpiry,
			double _riskFreeInterestRate, double _volatility, double _dividendYield) {
		double spotPrice = _spotPrice * exp((_riskFreeInterestRate - _dividendYield) * yearsToExpiry);

		int n = 1000;
		double deltaT = yearsToExpiry / n;

		double up = exp(_volatility * sqrt(deltaT));
		double down = 1.0 / up;

		double p0 = 0.5;
		double p1 = 1.0 - p0;
		double p[] = new double[n + 1];
		for (int i = 0; i <= n; ++i) {
			p[i] = strickPrice - spotPrice * pow(up, 2 * i - n);
			if (p[i] < 0)
				p[i] = 0;
		}
		// move to earlier times
		for (int j = n - 1; j >= 0; --j) {
			for (int i = 0; i <= j; ++i) {
				p[i] = p0 * p[i] + p1 * p[i + 1]; // binomial value
				// double exercise = strickPrice - spotPrice * pow(up, 2 * i -
				// j); // exercise value
				// if (p[i] < exercise) p[i] = exercise;
				// if (p[i] < 0) p[i] = 0;
			}
		}
		double ret = p[0];
		ret *= exp(-_riskFreeInterestRate * yearsToExpiry);

		return ret;
	}

	public double callOptionValue(double _spotPrice, double strickPrice, double yearsToExpiry,
			double _riskFreeInterestRate, double _volatility, double _dividendYield) {
		double spotPrice = _spotPrice * exp((_riskFreeInterestRate - _dividendYield) * yearsToExpiry);
		int n = 1000;
		double deltaT = yearsToExpiry / n;

		double up = exp(_volatility * sqrt(deltaT));
		double down = 1.0 / up;

		double p0 = 0.5;
		double p1 = 1.0 - p0;

		// initial values at time T
		double p[] = new double[n + 1];
		for (int i = 0; i <= n; ++i) {
			p[i] = spotPrice * pow(up, 2 * i - n) - strickPrice;
			if (p[i] < 0)
				p[i] = 0;
		}
		for (int j = n - 1; j >= 0; --j) {
			for (int i = 0; i <= j; ++i) {
				p[i] = p0 * p[i] + p1 * p[i + 1]; // binomial value

			}
		}

		double ret = p[0];
		ret *= exp(-_riskFreeInterestRate * yearsToExpiry);

		return ret;
	}

	/***************** Date Difference ****************/

	public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	/****** (St-k)+ ******/
	public double maxstk(double St, double K) {
		if (St > K)
			return St - K;
		else
			return 0;

	}

	/****** (K-St)+ ******/

	public double maxkst(double K, double St) {
		if (K >= St)
			return K - St;
		else
			return 0;

	}
	@Override
	public double currencyConvertion(String from, String to) {
		String response = HttpRequest.get("https://v3.exchangerate-api.com/bulk/428d417084fe51418dc991a4/" + from)
				.accept("application/json").body();
		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("rates");
		return status.getDouble(to);
	}

	@Override
	public Exchange currencyConvertion(String from) {
		Exchange exchange = new Exchange();
		String response = HttpRequest.get("https://v3.exchangerate-api.com/bulk/428d417084fe51418dc991a4/" + from)
				.accept("application/json").body();
		JSONObject jsonObject = new JSONObject(response);
		JSONObject status = jsonObject.getJSONObject("rates");
		exchange.setUsd(status.getDouble("USD") + "");
		exchange.setEur(status.getDouble("EUR") + "");
		exchange.setAud(status.getDouble("AUD") + "");
		exchange.setCad(status.getDouble("CAD") + "");
		exchange.setCny(status.getDouble("CNY") + "");
		exchange.setInr(status.getDouble("INR") + "");
		exchange.setGpb(status.getDouble("GBP") + "");
		exchange.setJpy(status.getDouble("JPY") + "");
		exchange.setRub(status.getDouble("RUB") + "");
		exchange.setTnd(status.getDouble("TND") + "");
		return exchange;

	}

}
