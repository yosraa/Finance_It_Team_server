package tn.esprit.Finance_It_Team_server.services;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.utils.Exchange;

@Remote
public interface CalculServiceRemote {
	double callPrice(double s, double x, double r, double sigma, double t); 
    double putPrice(double s, double x, double r, double sigma, double t); 
    double pdf(double x);
    double pdf(double x, double mu, double sigma);
    double cdf(double z); 
    double cdf(double z, double mu, double sigma);
    double Parity(double T, double S, double K, double r, double sigma, double q ,String Type);
	double putOptionValue(double _spotPrice, double strickPrice, double yearsToExpiry, double _riskFreeInterestRate, double _volatility, double _dividendYield)  ;
	double callOptionValue(double _spotPrice, double strickPrice, double yearsToExpiry, double _riskFreeInterestRate, double _volatility, double _dividendYield) ;
	long getDateDiff(Date date1, Date date2, TimeUnit timeUnit)	;
	double maxkst(double K,double St);
	double maxstk(double St,double K);
	Exchange currencyConvertion(String from);
	double currencyConvertion(String from, String to);


}
