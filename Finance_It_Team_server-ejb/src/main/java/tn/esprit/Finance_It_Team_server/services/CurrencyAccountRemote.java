package tn.esprit.Finance_It_Team_server.services;


import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.Trader;


@Stateless
@LocalBean
public class CurrencyAccountRemote implements ICurrencyAccount {
	
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;
	
	@Override
	public int addCurrecnyAccount (CurrencyAccount account ,int id )
	{
		em.persist(account);
		Trader tr = em.find(Trader.class, id);
		account.setTrader(tr);
		return account.getId();	
	}
	
	@Override
	public float readCurrecnyAccount(int id)
	{
		
		CurrencyAccount o = em.find(CurrencyAccount.class,id);
		return o.getAmount();
	}
	
	@Override
	public int deleteCurrecnyAccount(int id)
	{
		CurrencyAccount o = em.find(CurrencyAccount.class,id);
		em.remove(o);
		return id;
	
		
		
	}
	
	@Override
	public void updateCurrecnyAccount(CurrencyAccount account)
	{
		em.merge(account);
	}

	

	@Override
	public void affecterCurrencyAccountTrader(int currencyaccountId , int traderId) {
		Trader t= em.find(Trader.class,traderId);
		CurrencyAccount c = em.find(CurrencyAccount.class, currencyaccountId);
		c.setTrader(t);
		
	}
	
	@Override
	public List<CurrencyAccount> getAllCurrencyAccount() {
		TypedQuery<CurrencyAccount> query = em.createQuery("Select c from CurrencyAccount c", CurrencyAccount.class);
		

		return query.getResultList();
	}
	public List<Float> getAllCurrencyAccount1() {
		TypedQuery<Float> query = em.createQuery("Select c.amount from CurrencyAccount c", Float.class);
		

		return query.getResultList();
	}
	
	@Override
	public List<CurrencyAccount> getAllCurrencyOptionIdTrader(int t) {
		TypedQuery<CurrencyAccount> query = em.createQuery(
				"Select c from CurrencyAccount c where trader_id =:t ", CurrencyAccount.class);
		query.setParameter("t", t);
		return query.getResultList();
	}
	public float  CurrentProfit (int id)
	{
		float profit = 0;
		CurrencyAccount o = em.find(CurrencyAccount.class, id);

		o.getTypecurrency();
		if (o.getTypecurrency() == o.getTypecurrency().dollar) {
			
				profit=(float) ((o.getSumIncome()-o.getSumExpenses())*2.39182);
			} 
		else {
			
				if (o.getTypecurrency() == o.getTypecurrency().euro) {
				profit= (float)((o.getSumIncome()-o.getSumExpenses())*2.96635);

			}
				else {
					
					if (o.getTypecurrency() == o.getTypecurrency().pound) {
					profit= (float)((o.getSumIncome()-o.getSumExpenses())*3.32540);

				}
					else {
						
						if (o.getTypecurrency() == o.getTypecurrency().yen) {
						profit= (float)((o.getSumIncome()-o.getSumExpenses())*0.0224905);

					}
			
		}
		

				}
		}
		return profit;
	}
	

}

		
	

	



