package tn.esprit.Finance_It_Team_server.services;

import java.lang.Math;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.Finance_It_Team_server.services.CalculService;

@Stateless
@LocalBean
public class Greeks implements GreeksRemote {

	public double delta(double s, double k, double r, double sigma, double t, double q, String type) {
		double d1 = (Math.log(s / k) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));
		CalculService c1 = new CalculService();
		if (type.equalsIgnoreCase("call")) {
			return Math.exp(-q * t) * c1.cdf(d1);
		} else
			return -Math.exp(-q * t) * c1.cdf(-d1);
	}

	public double vega(double s, double k, double r, double sigma, double t, double q) {
		double d1 = (Math.log(s / k) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));
		CalculService c1 = new CalculService();

		return s * Math.exp(-q * t) * c1.cdf(d1) * Math.sqrt(t);
	}

	public double rho(double s, double k, double r, double sigma, double t, String type) {
		CalculService c1 = new CalculService();
		double d1 = (Math.log(s / k) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));

		double d2 = d1 - sigma * Math.sqrt(t);

		if (type.equalsIgnoreCase("call")) {
			return k * t * Math.exp(-r * t) * c1.cdf(d2);
		} else
			return -k * t * Math.exp(-r * t) * c1.cdf(-d2);
	}

	public double gamma(double s, double k, double r, double sigma, double t, double q) {
		CalculService c1 = new CalculService();
		double d1 = (Math.log(s / k) + (r + sigma * sigma / 2) * t) / (sigma * Math.sqrt(t));

		return Math.exp(-q * t) * c1.cdf(d1) / (s * sigma * Math.sqrt(t));

	}

}
