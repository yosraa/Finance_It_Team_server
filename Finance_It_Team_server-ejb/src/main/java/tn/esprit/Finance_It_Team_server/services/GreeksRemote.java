package tn.esprit.Finance_It_Team_server.services;

import javax.ejb.Remote;

@Remote
public interface GreeksRemote {
	public double delta(double s, double k, double r, double sigma, double t, double q, String type) ;
	public double vega(double s, double k, double r, double sigma, double t, double q);
	public double rho(double s, double k, double r, double sigma, double t,String type);
	public double gamma(double s, double k, double r, double sigma, double t, double q) ;
					

}
