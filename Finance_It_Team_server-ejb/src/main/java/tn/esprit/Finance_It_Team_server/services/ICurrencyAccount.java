package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;


@Remote
public interface ICurrencyAccount {
	public int addCurrecnyAccount (CurrencyAccount account,int id);
	public float readCurrecnyAccount(int id);
    public int deleteCurrecnyAccount(int id);
    public void updateCurrecnyAccount(CurrencyAccount account);
    public void affecterCurrencyAccountTrader(int currencyaccountId , int traderId);
    public List<CurrencyAccount> getAllCurrencyAccount();
	public float  CurrentProfit (int id);
	List<CurrencyAccount> getAllCurrencyOptionIdTrader(int t);
	public List<Float> getAllCurrencyAccount1();
}
