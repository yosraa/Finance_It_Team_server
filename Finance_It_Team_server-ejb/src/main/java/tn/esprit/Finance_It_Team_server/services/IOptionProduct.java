package tn.esprit.Finance_It_Team_server.services;


import java.util.List;
import javax.ejb.Remote;


import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;


@Remote
public interface IOptionProduct {
	public int addOptionProduct(CurrencyOption option, int id);

	public int deleteOptionProduct(int id);

	public void updateOption(CurrencyOption option);

	public float readOptionProductt(int id);

	public List<CurrencyOption> getAllCurrencyOption();

	// public float readOptionProduct2(int id);

	public void affecterTraderOption(int TraderId, int CurrencyOptionId);


	public List<CurrencyOption> getCurrencyOptionById();

	public List<CurrencyOption> getCurrencyOptionById1();

	public List<CurrencyOption> getAvailableEtat();

	public List<CurrencyOption> getUnvailableEtat();

	public float gainOrLossOfTraderCallAndPut(int id);

	public List<CurrencyOption> findOptionByUnitExchange(UnitExchange t);

	public long calculerUnitEur_TND();

	public long calculerUnitJPY_EUR();

	public long calculerUnitUSD_EUR();

	public int deleteOptionUser(int id);

	public long calculerUnitCAD_EUR();

	public long calculerUnitUSD_TDN();

	public List<Object[]> StrikePriceOfThisMonth1();
	public List<Object[]> CurrentPriceOfThisMonth1();
	public List<Object[]> StrikePriceOfThisMonth2();

	public List<CurrencyOption> getAllCurrencyOptionNotIdTrader(int id);

	public List<CurrencyOption> TreeOfCurrencyByRisk();

	public List<CurrencyOption> getAllCurrencyOptionIdTrader(int t);

	public List<Float> getAmountFromCurrencyAccount();

	public long countRisk();

	public long countRiskMax();
	public long countRiskMin();

	public List<CurrencyOption> getAllCurrencyOption1();
	public List<Object[]> CurrentPriceOfThisMonth2();
	public void currencyConvertion(String from,String to);

	public List<CurrencyOption> getoptionById(int id);
	public int UserConnecte() ;

	public long calculerUnit();
	public void deleteOptionProduct1(CurrencyOption cu);
}
