package tn.esprit.Finance_It_Team_server.services;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.Portfolio;

@Remote
public interface IPortfolioService {
	
	public int addPortfolio (Portfolio portfolio);
    public int deletePortfolio(int id);
    public void updatePortfolio(Portfolio portfolio);
	public String showPortfolio(int id);	 
	
}
