package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.RealData;

@Remote
public interface IRealData  {

	
	int addRealData (RealData re);
	float readRealData(int id);
	public List<RealData> getAllrealdata();
}
