package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import tn.esprit.Finance_It_Team_server.entities.Stocks;

public interface IRealTimeDataS {
	public List<Stocks> getGeneralStocks();
}
