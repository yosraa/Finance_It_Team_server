package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.Remote;

import javafx.util.Callback;
import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;

@Remote
public interface IStockOptionService {
	public int addStockOption (StockOption stockOption);
    public int deleteStockOption(int id);
    public void updateStockOption(StockOption stockOption);
	public float showStockOption(int id);
	
    public List<Object[]> StrikePriceOfThisMonthStockOption();

	public List<StockOption> getNoStatus();
	public List<StockOption> getYesStatus();

	public List<StockOption> getAllStockOption();
	public void affectAStockOptionToTrader(int stockOptionId , int traderId);

	public List<StockOption> findAllStockOptions();
	
	
	List<StockOption> getChecked();
	List<StockOption> getNonChecked();
	StockOption getIdeeById(Integer i);
	List<StockOption> getIdeesByUserId(Integer i);
	//List<StockOption> findOptionById(float nbShares);
	float  StockOptionPutCall(int id);


	float calculValueShares(int id);
	float calculValueShares(float a,float b,float c);
	 float futuresPrice(int id);
	 float StockOptionTimeValue(int id);
	 float OptionPriceFormula(int id);
	 String DeepInOutAtTheMoney(int id);
	 float PriceOfALookbackOption(float S,float Smin,float r,float q,float sigma,float time);
	 float WarrantPriceAdjustedBS(int id);
	void UpdateOptionStatus(int id, StateOptionStock status);
	int addStockOption(StockOption stockOption, int id);
	List<StockOption> getAllStockOptionNotATrader(int t);
	public List<StockOption> findOptionsByTrader(int id);
	List<StockOption> getAllStockOptionbyStock(int t);
	StockOption findStockOptioneById(int id);
	int updateStockOptionEtTrader(StockOption stockOption, int id);
	
	

}
