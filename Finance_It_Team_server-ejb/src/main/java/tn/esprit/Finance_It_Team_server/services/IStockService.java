package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;

@Remote
public interface IStockService {
	public int addStock (Stock stock);
    public int deleteStock(int id);
    public void updateStock(Stock stock);
	public float showStock(int id);	
	//---------------------------------------
	
	public List<Stock> getAllStock();
	public void affectAStockToAStockOption(int stockId , int stockOptionId);
	public List<Stock> findAllStock();
}
