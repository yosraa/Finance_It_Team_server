package tn.esprit.Finance_It_Team_server.services;

import java.util.Comparator;

public class MyComp implements Comparator<String>{
	@Override
	public int compare(String o1, String o2) {
		return o2.compareTo(o1);
	}
}
