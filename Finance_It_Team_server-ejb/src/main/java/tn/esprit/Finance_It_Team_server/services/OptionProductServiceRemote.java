package tn.esprit.Finance_It_Team_server.services;

import java.util.List;
import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;

import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;


@Stateless
@LocalBean
public class OptionProductServiceRemote implements IOptionProduct {

	@PersistenceContext(unitName = "Finance_It_Team_server-ejb")
	EntityManager em;

	@Override
	public int addOptionProduct(CurrencyOption optionp, int id) {

		em.persist(optionp);
		Trader tr = em.find(Trader.class, id);
		optionp.setTrader(tr);
		return id;

	}

	@Override
	public int deleteOptionUser(int id) {
		Trader t = em.find(Trader.class, id);
		em.remove(t);

		return id;
	}

	@Override
	public void affecterTraderOption(int TraderId, int CurrencyOptionId) {
		Trader t = em.find(Trader.class, TraderId);
		CurrencyOption c = em.find(CurrencyOption.class, CurrencyOptionId);
		System.out.println("this is the option of trader 1");
		c.setTrader(t);

	}

	@Override
	public float readOptionProductt(int id) {

		CurrencyOption o = em.find(CurrencyOption.class, id);
		return o.getCurrentPrice();
	}

	@SuppressWarnings("unchecked")
	public Collection<CurrencyOption> findAllOptions() {
		Query query = em.createQuery("SELECT * FROM currencyoption ");
		return (Collection<CurrencyOption>) query.getResultList();
	}

	public float readOptionProduct2(int id) {

		CurrencyOption o = em.find(CurrencyOption.class, id);
		CurrencyOption option2 = new CurrencyOption();
		option2.setCurrentPrice(o.getCurrentPrice());
		option2.setRisk(o.getRisk());
		option2.setTime(o.getTime());
		option2.setVolatility(o.getVolatility());
		option2.setStrikePrice(o.getStrikePrice());
		return option2.getCurrentPrice();

	}

	@Override
	public int deleteOptionProduct(int id) {
		CurrencyOption o = em.find(CurrencyOption.class, id);
		em.remove(o);

		return id;
	}
	public void deleteOptionProduct1(CurrencyOption cu) {
		CurrencyOption o = em.find(CurrencyOption.class,cu);
		em.remove(o);

	
	}

	@Override
	public void updateOption(CurrencyOption option) {
		em.merge(option);
	}

	@Override
	public List<CurrencyOption> getAllCurrencyOption() {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption c where trader_id =:t",
				CurrencyOption.class);

		return query.getResultList();
	}
	@Override
	public List<CurrencyOption> getoptionById(int id) {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption c where c.id=:id",
				CurrencyOption.class);
		query.setParameter("id", id);
		List<CurrencyOption>li=query.getResultList();
		
		return li;
	}
	@Override
	public List<CurrencyOption> getAllCurrencyOption1() {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption c ",
				CurrencyOption.class);

		return query.getResultList();
	}

	@Override
	public List<CurrencyOption> getAllCurrencyOptionNotIdTrader(int t) {
		TypedQuery<CurrencyOption> query = em.createQuery(
				"Select c from CurrencyOption c where trader_id !=:t and Archive='0'", CurrencyOption.class);
		query.setParameter("t", t);
		return query.getResultList();
	}

	@Override
	public List<CurrencyOption> getAllCurrencyOptionIdTrader(int t) {
		TypedQuery<CurrencyOption> query = em.createQuery(
				"Select c from CurrencyOption c where trader_id =:t and Archive='0'", CurrencyOption.class);
		query.setParameter("t", t);
		return query.getResultList();
	}

	@Override
	public List<CurrencyOption> getCurrencyOptionById() {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption as c WHERE Archive ='0'",
				CurrencyOption.class);
		return query.getResultList();
	}

	@Override
	public List<CurrencyOption> getCurrencyOptionById1() {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption as c WHERE Archive ='1'",
				CurrencyOption.class);
		return query.getResultList();
	}

	@Override
	public List<CurrencyOption> getAvailableEtat() {
		TypedQuery<CurrencyOption> query = em.createQuery(
				"Select c from CurrencyOption c " + "where StateOption like '%available%' ", CurrencyOption.class);
		List<CurrencyOption> optiona = query.getResultList();
		return optiona;
	}

	@Override
	public List<CurrencyOption> getUnvailableEtat(){
		TypedQuery<CurrencyOption> query = em.createQuery(
				"Select c from CurrencyOption c " + "where StateOption like '%unavailable%' ", CurrencyOption.class);
		List<CurrencyOption> optionu = query.getResultList();
		return optionu;
	}

	@Override
	public float gainOrLossOfTraderCallAndPut(int id) {
		float k = 0;
		CurrencyOption o = em.find(CurrencyOption.class, id);

		o.getTypeoption();
		if (o.getTypeoption() == TypeOption.call) {
			if (o.getCurrentPrice() > o.getStrikePrice()) {
				k = (float) (o.getCurrentPrice() - o.getStrikePrice() - o.getPremium());
			} else {
				k = (float) -o.getPremium();
			}
		} else {
			o.getTypeoption();
			if (o.getTypeoption() == TypeOption.put) {
				if (o.getStrikePrice() > o.getCurrentPrice()) {
					k = (float) (o.getStrikePrice() - o.getCurrentPrice() - o.getPremium());
				} else {
					k = (float) -o.getPremium();
				}
			}
		}

		return k;

	}

	@Override
	public List<CurrencyOption> findOptionByUnitExchange(UnitExchange t) {
		TypedQuery<CurrencyOption> query = em
				.createQuery("SELECT e FROM" + " CurrencyOption e WHERE e.unitEchange =:param", CurrencyOption.class);
		query.setParameter("param", t);
		return (query.getResultList());
	}

	@Override
	public long calculerUnitEur_TND() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c) FROM CurrencyOption c WHERE c.unitEchange =:param");
		query.setParameter("param", UnitExchange.EUR_TND);
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	@Override
	public long calculerUnitCAD_EUR() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c) FROM CurrencyOption c WHERE c.unitEchange =:param");
		query.setParameter("param", UnitExchange.CAD_EUR);
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	@Override
	public long calculerUnitUSD_TDN() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c) FROM CurrencyOption c WHERE c.unitEchange =:param");
		query.setParameter("param", UnitExchange.USD_TDN);
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	@Override
	public long calculerUnitJPY_EUR() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c) FROM CurrencyOption c WHERE c.unitEchange =:param");
		query.setParameter("param", UnitExchange.JPY_EUR);
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	@Override
	public long calculerUnitUSD_EUR() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c) FROM CurrencyOption c WHERE c.unitEchange =:param");
		query.setParameter("param", UnitExchange.USD_EUR);
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	@Override
	public long calculerUnit() {
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(c.unitEchange) FROM CurrencyOption c ");
		
		long nombre = (long) query.getSingleResult();
		return nombre;

	}

	public List<Object[]> StrikePriceOfThisMonth1() {
		Query query = em.createQuery(
				"SELECT  SUM(c.strikePrice), c.expirationDate FROM CurrencyOption c WHERE c.typeoption like '%call%' group by c.expirationDate ");
		@SuppressWarnings("unchecked")
		List<Object[]> l = query.getResultList();
		return l;
	}

	public List<Object[]> StrikePriceOfThisMonth2() {
		Query query = em.createQuery(
				"SELECT  SUM(c.strikePrice), c.expirationDate FROM CurrencyOption c WHERE c.typeoption like '%put%' group by c.expirationDate ");

		@SuppressWarnings("unchecked")
		List<Object[]> l = query.getResultList();
		return l;
	}

	@Override
	public List<Object[]> CurrentPriceOfThisMonth2() {
		Query query = em.createQuery(
				"SELECT  SUM(c.currentPrice), c.expirationDate FROM CurrencyOption c WHERE c.typeoption like '%put%' group by c.expirationDate ");

		@SuppressWarnings("unchecked")
		List<Object[]> l = query.getResultList();
		return l;
	}
	@Override
	public List<Object[]> CurrentPriceOfThisMonth1() {
		Query query = em.createQuery(
				"SELECT  SUM(c.currentPrice), c.expirationDate FROM CurrencyOption c WHERE c.typeoption like '%call%' group by c.expirationDate ");

		@SuppressWarnings("unchecked")
		List<Object[]> l = query.getResultList();
		return l;
	}
	@Override
	public List<CurrencyOption> TreeOfCurrencyByRisk() {
		TypedQuery<CurrencyOption> query = em.createQuery("Select c from CurrencyOption c order by c.risk desc ",
				CurrencyOption.class);
		List<CurrencyOption> optionu = query.getResultList();
		return optionu;
	}
	@Override
	public List<Float> getAmountFromCurrencyAccount() {
		TypedQuery<Float> query = em.createQuery("Select c.amount from CurrencyAccount c", Float.class);
		

		return query.getResultList();
	}
	@Override
	public long countRisk() { 
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(risk) FROM CurrencyOption c");
				long nombre = (long) query.getSingleResult();
		return nombre;

	}
	@Override
	public long countRiskMax() { 
		javax.persistence.Query query = em
				.createQuery("SELECT COUNT(risk) FROM CurrencyOption c where c.risk=(SELECT MAX(c.risk) from CurrencyOption c)");
				long nombre = (long) query.getSingleResult();
		return nombre;

	}
	public long countRiskMin(){javax.persistence.Query query = em
			.createQuery("SELECT COUNT(risk) FROM CurrencyOption c where c.risk=(SELECT MIN(c.risk) from CurrencyOption c)");
			long nombre = (long) query.getSingleResult();
	return nombre;}
	
	public void currencyConvertion(String from,String to)
	{
		String response = HttpRequest
				.get("https://v3.exchangerate-api.com/bulk/428d417084fe51418dc991a4/"+from)
				.accept("application/json").body();
		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("rates");
		Double eur = status.getDouble(to);
		System.out.println(eur*200);
	}
	public int UserConnecte() { 
		javax.persistence.Query query = em
				.createQuery("SELECT u.id FROM User u");
				int nombre = (int) query.getSingleResult();
		return nombre;

	}

}