package tn.esprit.Finance_It_Team_server.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.Finance_It_Team_server.entities.Portfolio;

@Stateless
public class PortfolioServiceRemote implements IPortfolioService{
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;
	@Override
	public int addPortfolio(Portfolio portfolio)
	{
	em.persist(portfolio);
	return portfolio.getId();
	}
	@Override
	public int deletePortfolio(int id) {
		Portfolio p = em.find(Portfolio.class,id);
		em.remove(p);
		return id;
	}
	@Override
	public void updatePortfolio(Portfolio portfolio) {
		em.merge(portfolio);		
	}
	@Override
	public String showPortfolio(int id) {
		
		Portfolio p = em.find(Portfolio.class,id);
		String x=p.getName();
				return x;		
	}
}
