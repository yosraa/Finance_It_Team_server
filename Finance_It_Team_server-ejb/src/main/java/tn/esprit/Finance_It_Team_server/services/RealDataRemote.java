package tn.esprit.Finance_It_Team_server.services;



import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.RealData;

@Stateless
@LocalBean
public class RealDataRemote implements IRealData{
	
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;

	@Override
	public int addRealData(RealData re) {
			em.persist(re);
			
		return re.getId();
	}

	@Override
	public float readRealData(int id) {

		RealData o = em.find(RealData.class,id);
		return o.getId();
	}
	@Override
	public List<RealData> getAllrealdata() {
		//TypedQuery<RealData> query = em.createQuery("Select c from realdata c", RealData.class);
		Query query = em.createQuery("select p from " + RealData.class.getName() + " p");

		return query.getResultList();
	}
	
	
	
	
	

}
