package tn.esprit.Finance_It_Team_server.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import com.github.kevinsawicki.http.HttpRequest;

import tn.esprit.Finance_It_Team_server.entities.Stocks;
import tn.esprit.Finance_It_Team_server.services.MyComp;

import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.Set;

import org.json.JSONObject;

@Stateless
public class RealTimeDataServiceS implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(RealTimeDataServiceS.class.getName());

	public static List<Stocks> getGeneralStocks() {
		List<Stocks> generalStocks = new ArrayList<>();
		String[] symbolsContainer = { "AMZN", "AAPL", "BAC", "BMW", "DIS", "INTC", "GOOG", "NFLX", "MSFT", "WMT" };
		String[] namesContainer = { "amazon", "apple", "bank of america corp", "bayerische motoren werke ag",
				"walt disney co", "intel corporation", "google", "netflix", "microsoft", "walmart" };
		for (int i = 0; i < symbolsContainer.length; i++) {

			if (i % 5 == 0 && i != 0)
				try {
					System.out.println("System is Sleeping...");
					TimeUnit.SECONDS.sleep(60);
				} catch (InterruptedException e) {
					LOGGER.log(Level.SEVERE, "System could not sleep.");
				}

			
			  String response = HttpRequest.get(
			 "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol="
			 + symbolsContainer[i] +
			  "&interval=1min&apikey=02T4VFNIZQ1QHZ0H").accept(
			  "application/json").body();
			 
		
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.has("Monthly Time Series")) {
				LOGGER.log(Level.INFO, "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol="
						+ symbolsContainer[i] + "&interval=1min&apikey=02T4VFNIZQ1QHZ0H");
				TreeSet<String> sortedKeysToFetchFrom = new TreeSet<>(new MyComp());
				Set<String> keysToFetchFrom = ((JSONObject) jsonObject.get("Monthly Time Series")).keySet();
				sortedKeysToFetchFrom.addAll(keysToFetchFrom);
				String[] keys = new String[2];
				int index = 0;
				for (String K : sortedKeysToFetchFrom) {
					if (index == 2)
						break;
					keys[index] = K;
					index++;
				}
				JSONObject result = ((JSONObject) ((JSONObject) jsonObject.get("Monthly Time Series")).get(keys[0]));
				JSONObject resultPrevious = ((JSONObject) ((JSONObject) jsonObject.get("Monthly Time Series"))
						.get(keys[1]));

				Stocks s = new Stocks();
				s.setSymbol(symbolsContainer[i]);
				s.setName(namesContainer[i]);
				s.setOpen((result.getString("1. open")));
				s.setHigh( (result.getString("2. high")));
				s.setLow((result.getString("3. low")));
				s.setClose( (result.getString("4. close")));
				s.setVolume( (result.getString("5. volume")));
				generalStocks.add(s);
			} else {
				LOGGER.log(Level.SEVERE, "Symbol " + symbolsContainer[i] + " not fetched by Alpha Vantage");
				LOGGER.log(Level.SEVERE, "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol="
						+ symbolsContainer[i] + "&interval=1min&apikey=02T4VFNIZQ1QHZ0H");
			}
		}
		return generalStocks;
	}
}
