package tn.esprit.Finance_It_Team_server.services;


import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.entities.Reclamation;



@Stateless
@LocalBean
public class ServiceReclamation implements ServiceReclamationRemote {
	private static final String ACTION_1 = "SELECT x FROM ";
	private static final String ACTION_2 = "param";
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;
	
	
	@Override
	public void ajouterReclamation(Reclamation reclamation) {
		reclamation.setState("Not Viewed");
		em.persist(reclamation);
	}

	@Override
	public List<Reclamation> listerReclamations() {
		TypedQuery<Reclamation> query = em.createQuery("Select c from " + Reclamation.class.getName() + " c ",
				Reclamation.class);
		return query.getResultList();
	}

	@Override
	public List<Reclamation> listerReclamationParTrader(int idTrader) {
		TypedQuery<Reclamation> query = em.createQuery(ACTION_1+ Reclamation.class.getName() +" as x where x.traderId=:param",
				Reclamation.class);
    	query.setParameter(ACTION_2, idTrader);

		return query.getResultList();
	}

	@Override
	public void modifyState(int id) {
		  Reclamation reclamation = em.find(Reclamation.class, id);
		  reclamation.setState("viewed");
		  em.merge(reclamation);
	}

	@Override
	public List<Reclamation> listerReclamationsViewed() {
		TypedQuery<Reclamation> query = em.createQuery(ACTION_1 + Reclamation.class.getName() +" as x where x.state=:param",
				Reclamation.class);
    	query.setParameter(ACTION_2, "viewed");

		return query.getResultList();
	}

	@Override
	public List<Reclamation> listerReclamationsNotViewed() {
		TypedQuery<Reclamation> query = em.createQuery(ACTION_1 + Reclamation.class.getName() +" as x where x.state=:param",
				Reclamation.class);
    	query.setParameter(ACTION_2, "not viewed");

		return query.getResultList();
	}

	

}
