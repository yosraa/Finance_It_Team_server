package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.entities.Trader;

@Remote
public interface ServiceReclamationRemote {
	public void ajouterReclamation(Reclamation reclamation);
	public List<Reclamation> listerReclamations();
	public List<Reclamation> listerReclamationsViewed();
	public List<Reclamation> listerReclamationsNotViewed();
	public List<Reclamation> listerReclamationParTrader(int idTrader);
	public void modifyState(int id);
}
