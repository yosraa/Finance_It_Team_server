package tn.esprit.Finance_It_Team_server.services;

import tn.esprit.Finance_It_Team_server.entities.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.services.PasswordAuthentication;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Stateless
@LocalBean
public class ServiceUser implements ServiceUserRemote {
	private static final String ACTION_1 = "SELECT x FROM ";
	private static final String ACTION_2 = "param";
	private static final String ACTION_3 =" as x where x.statusTrader=:param";
	private static final String ACTION_4 ="Accepted";
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;
	
	public void addLoginLog(int Userid) {
	LoginLogs loginLog=new LoginLogs();
	Date date=new Date();
	
	loginLog.setDate(date);
	loginLog.setDateFin(date);	
	loginLog.setTraderId(Userid);
	em.persist(loginLog);
	}
	
	/**
	 * 
	 */
	@Override
	public void updateDateFinLoginLog(int userId) {
		//rec...
		Date date=new Date();
		TypedQuery<LoginLogs> query = em.createQuery(ACTION_1 + LoginLogs.class.getName() +" as x where x.traderId=:param",
				LoginLogs.class);
    	query.setParameter(ACTION_2, userId);
    	List<LoginLogs> list=query.getResultList();
    	LoginLogs log=list.get(list.size()-1);
    	log.setDateFin(date);
    	log.setMinutes(getDateDiff(log.getDate(),date,TimeUnit.MINUTES));
    	em.merge(log);
    	
    	
		}
	@Override
	public long allMinutesByday(LocalDate date, int userId){
		TypedQuery<LoginLogs> query = em.createQuery(ACTION_1 + LoginLogs.class.getName() +" as x where x.traderId=:param",
				LoginLogs.class);
    	query.setParameter(ACTION_2, userId);
    	List<LoginLogs> list=query.getResultList();
    	long sum=0;
    	for (LoginLogs l : list) {
			if(date.getYear()==l.getDate().getYear()+1900&&date.getMonthValue()-1==l.getDate().getMonth()&&date.getDayOfMonth()==l.getDate().getDate())
			{
				sum=sum+l.getMinutes();
			}
    	}
    	return sum;
	}
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	@SuppressWarnings("deprecation")
	@Override
	public void addUser(User u) {
		PasswordAuthentication passwordAuthentication=new PasswordAuthentication();
		if(usernameExists(u.getUserName())==false && emailExists(u.getEmail())==false){
		u.setPassword(passwordAuthentication.hash(u.getPassword()));
		if(u instanceof Trader){
			((Trader) u).setStatusTrader("pending");
			em.persist((Trader)u);
		}
		else{
			em.persist((Admin)u);
		}
		}
	}

	
	
	@Override
	public void deleteUser(User u,int id) {

		
		
		if(u instanceof Trader){
			Trader trader = em.find(Trader.class, id);


			  em.remove(trader);

		}
		else{
			Admin admin = em.find(Admin.class, id);


			  em.remove(admin);

		}
	}



	

	@Override
	public Trader findUser(int id){
		Trader trader = em.find(Trader.class, id);
		return trader;
	}


	@Override
	public void modifyUser(User u, int id) {
		
		if(u instanceof Trader){
			Trader trader = em.find(Trader.class, id);

			  trader.setStatusTrader(((Trader) u).getStatusTrader());
			  em.merge(trader);
		}
		else{
			Admin admin = em.find(Admin.class, id);
				admin.setFirstName(u.getEmail());
			  em.merge(admin);

		}
		
	}
	@Override
	public List<User> getAllUsers() {
		TypedQuery<User> query = em.createQuery("Select c from " + User.class.getName() + " c ",
				User.class);

		return query.getResultList();
	}
	@Override
	public List<Trader> getAllTraders() {
		TypedQuery<Trader> query = em.createQuery("Select c from " + Trader.class.getName() + " c ",
				Trader.class);

		return query.getResultList();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	
	public List<Integer> login(String userName, String password){
		PasswordAuthentication passwordAuthentication=new PasswordAuthentication();
		ArrayList<User> allUsers=(ArrayList<User>) getAllUsers();
		ArrayList<Integer> idAndRole=new ArrayList<>();
		int id=-1;
		int role=-1;
		String status=ACTION_4;
		String email="";
		for(User user :allUsers){
			if(user.getUserName().equals(userName) && 
					passwordAuthentication.authenticate(password,user.getPassword()))
			{
				email=user.getEmail();
				id=user.getId();
				if (user instanceof Trader) role=1;
				else role=2;
				if(role==1)status=((Trader)user).getStatusTrader();
				break;
			}
		}
		if(status.equals(ACTION_4)){
		idAndRole.add(id);
		idAndRole.add(role);
		addLoginLog(id);
		return idAndRole;
		}
		if(id==-1||role==-1){
			//sendMailWithNewPassword(email);
		}
		idAndRole.add(-1);
		idAndRole.add(-1);
		return idAndRole;
	}



	@Override
	public User getUser(int id) {
		return em.find(User.class, id);
	}



	@Override
	public void modifyUser2(User u, int id) {
			em.merge(u);
					
	}

	

	@SuppressWarnings("deprecation")
	@Override
	public void sendMailWithNewPassword(String email) {
		
		String newPassWord=Long.toHexString(Double.doubleToLongBits(Math.random()));
		TypedQuery<Trader> query = em.createQuery("SELECT x FROM " + Trader.class.getName() +" as x where x.email=:param",
				Trader.class);
    	query.setParameter("param",email);
    	int idUser=query.getResultList().get(0).getId();

    	Trader trader = em.find(Trader.class, idUser);
    	PasswordAuthentication passwordAuthentication=new PasswordAuthentication();

		trader.setPassword(passwordAuthentication.hash(newPassWord));
    	em.merge(trader);
    	
        String destination =email;
        String sujet="New Password";
        String msg="New Password has been requested : "+newPassWord;
        String login="investinvest02@gmail.com";
        String m2p="investinvest0";
        SendMail.send(destination, sujet, msg, login, m2p);
		
	}
	@Override
	public List<Trader> listBannedTraders() {
		TypedQuery<Trader> query = em.createQuery(ACTION_1 + Trader.class.getName() +ACTION_3,
				Trader.class);
    	query.setParameter(ACTION_2, "banned");

		return query.getResultList();
	}
	@Override
	public List<Trader> listPendingTraders() {
		TypedQuery<Trader> query = em.createQuery(ACTION_1 + Trader.class.getName() +ACTION_3,
				Trader.class);
    	query.setParameter(ACTION_2, "pending");

		return query.getResultList();
	}
	@Override
	public List<Trader> listAcceptedTraders() {
		TypedQuery<Trader> query = em.createQuery(ACTION_1 + Trader.class.getName() +ACTION_3,
				Trader.class);
    	query.setParameter(ACTION_2, ACTION_4);

		return query.getResultList();
	}

	@Override
	public List<LoginLogs> listLoginLogsOfTrader(int TraderId) {
		TypedQuery<LoginLogs> query = em.createQuery(ACTION_1 + LoginLogs.class.getName() +" as x where x.traderId=:param ",
				LoginLogs.class);
    	query.setParameter("param",TraderId);
    	return query.getResultList();		
	}
	@Override
	public boolean emailExists(String email) {
		TypedQuery<Trader> query = em.createQuery(ACTION_1 + Trader.class.getName() +" as x where x.email=:param ",
				Trader.class);
    	query.setParameter(ACTION_2, email);

    	if(query.getResultList().isEmpty())return false;
    	return true;
	}
	@Override
	public boolean usernameExists(String username) {
		TypedQuery<Trader> query = em.createQuery(ACTION_1 + Trader.class.getName() +" as x where x.userName=:param ",
				Trader.class);
    	query.setParameter(ACTION_2, username);

    	if(query.getResultList().isEmpty())return false;
    	return true;
	}
	
	public void cleanLogs(){
        Query q = em.createQuery("DELETE FROM loginlogs");
        q.executeUpdate(); 
	}
	
	public void sendMailIfUserDidNotLoginFor(int numberOfDays){
		LocalDate curruntDate=LocalDate.now();

		List<Trader> allTraders=getAllTraders();
		for(Trader trader : allTraders){
			int sum=0;
			for(int i=0 ;i<numberOfDays;i++){
			sum+=dateFoundInLogs(curruntDate.minusDays(i), trader.getId());
			}
			if(sum==0){
				System.out.println("emails :"+trader.getEmail());
		        String destination =trader.getEmail();
		        String sujet="Remainder";
		        String msg="this is a remainder for not logging in for more than "+numberOfDays+" days";
		        String login="investinvest02@gmail.com";
		        String m2p="investinvest0";
		        SendMail.send(destination, sujet, msg, login, m2p);
			
			}
		}
	}
	
	
	
	
	public int dateFoundInLogs(LocalDate date ,int id){
		List<LoginLogs> list=listLoginLogsOfTrader(id);
		for (LoginLogs l : list) {
			if(date.getYear()==l.getDate().getYear()+1900&&date.getMonthValue()-1==l.getDate().getMonth()&&date.getDayOfMonth()==l.getDate().getDate())
			{
				return 1;
			}
			
    	}
		return 0;
	}
}

