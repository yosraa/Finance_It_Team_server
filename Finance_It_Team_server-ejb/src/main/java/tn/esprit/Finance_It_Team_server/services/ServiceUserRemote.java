package tn.esprit.Finance_It_Team_server.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import tn.esprit.Finance_It_Team_server.entities.*;
@Remote
public interface ServiceUserRemote {
	public void addUser(User u);
	public void deleteUser(User u,int id);
	public List<Integer> login(String userName, String password);
	public List<User> getAllUsers();
	public Trader findUser(int id);
	public void modifyUser(User u, int id);
	public User getUser(int id);
	public void modifyUser2(User u, int id);
	public void sendMailWithNewPassword(String email);
	public List<Trader> listAcceptedTraders(); 
	public List<Trader> listBannedTraders();
	public List<Trader> listPendingTraders();
	public List<LoginLogs> listLoginLogsOfTrader(int TraderId);
	public List<Trader> getAllTraders();
	public void updateDateFinLoginLog(int userId);
	public long allMinutesByday(LocalDate date, int userId);
	boolean emailExists(String email);
	boolean usernameExists(String username);


}