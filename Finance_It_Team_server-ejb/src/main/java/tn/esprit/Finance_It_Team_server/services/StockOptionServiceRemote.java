package tn.esprit.Finance_It_Team_server.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;

@Stateless
@LocalBean
public class StockOptionServiceRemote implements IStockOptionService {
    public static User static_user = null;

	public static User getStatic_user() {
		return static_user;
	}
	public static void setStatic_user(User static_user) {
		StockOptionServiceRemote.static_user = static_user;
	}

	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;

	@Override
	public int addStockOption(StockOption stockOption) {
		em.persist(stockOption);
		return stockOption.getId();
	}
	@Override
	public List<StockOption> getAllStockOptionNotATrader(int t) {
		TypedQuery<StockOption> query = em.createQuery("Select c from StockOption c where trader_id !=:t ", StockOption.class);
		query.setParameter("t", t);
		return query.getResultList();
	}
	@Override
	public List<StockOption> getAllStockOptionbyStock(int t) {
		TypedQuery<StockOption> query = em.createQuery("Select c from StockOption c where stock_id =:t ", StockOption.class);
		query.setParameter("t", t);
		return query.getResultList();
	}
	
	@Override
	public StockOption findStockOptioneById(int id) {
		return em.find(StockOption.class, id);
	}
	@Override
	public List<StockOption> findOptionsByTrader(int id) {
		TypedQuery<StockOption> query = em.createQuery("select o from StockOption o where o.trader =:o ", StockOption.class);
		query.setParameter("o", em.find(Trader.class, id));
		return query.getResultList();
	}
	@Override
	public int addStockOption(StockOption stockOption,int id) {
	
		em.persist(stockOption);
		Trader tr=em.find(Trader.class, id);
		stockOption.setTrader(tr);
		return id;
	}
	@Override
	public int deleteStockOption(int Id) {
	/*	StockOption so = em.find(StockOption.class,id);
		em.remove(so);
		return id;*/
		StockOption so = em.find(StockOption.class,Id);

		em.remove(em.find(StockOption.class, Id));
		 
			return Id;
	}

	@Override
	public void updateStockOption(StockOption stockOption) {
		em.merge(stockOption);			
	}
	@Override
	public int updateStockOptionEtTrader(StockOption stockOption,int id) {
		Trader tr=em.find(Trader.class, id);
		stockOption.setTrader(tr);
		em.merge(stockOption);			
		return id;	
	}
	

	@Override
	public float showStockOption(int id) {
		StockOption so = em.find(StockOption.class,id);
		return so.getNbShares();
			}

	@Override
	public void affectAStockOptionToTrader(int stockOptionId, int traderId) {
		Trader t= em.find(Trader.class,traderId);
		StockOption so = em.find(StockOption.class,stockOptionId);
		so.setTrader(t);		
	}

	public List<StockOption> getAllStockOption() {
				
		TypedQuery<StockOption> query = em.createQuery("Select so from " + StockOption.class.getName() + " so ",
						StockOption.class);

				return query.getResultList();
	}

	
	public List<StockOption> findAllStockOptions() {
	
			TypedQuery  query = em.createQuery("SELECT x FROM stockoption as x",StockOption.class);
		List<StockOption> resultList = query.getResultList();
		return  resultList;
	}

	public List<StockOption> getNoStatus() {
		TypedQuery<StockOption> query = em.createQuery(
				"Select c from StockOption c " + "where StateOptionStock like '%no%' ", StockOption.class);
		List<StockOption> optionu = query.getResultList();
		return optionu;
	}
	@Override
	public List<StockOption> getYesStatus() {
		TypedQuery<StockOption> query = em.createQuery(
				"Select c from StockOption c " + "where StateOptionStock like '%yes%' ", StockOption.class);
		List<StockOption> optionu = query.getResultList();
		return optionu;
	}
	 @Override
	    public List<StockOption> getChecked() {
		 TypedQuery<StockOption> query = em.createQuery(
					"Select c from StockOption c " + "where StateOptionStock like '%yes%' ", StockOption.class);
			List<StockOption> optiona = query.getResultList();
			return optiona;
	     
			
	    }
	    @Override
	    public List<StockOption> getNonChecked() {
	    	 TypedQuery<StockOption> query = em.createQuery(
						"Select c from StockOption c " + "where StateOptionStock like '%no%' ", StockOption.class);
				List<StockOption> optiona = query.getResultList();
				return optiona;
	    		    }


	    @Override
	    public StockOption getIdeeById(Integer i) {
	    	   StockOption idea = null;
	    	  TypedQuery  query = em.createQuery("SELECT x FROM stockoption as x where x.id=?",StockOption.class);
		        
		        List<StockOption> resultList = query.getResultList();
				
	        return idea;
	    }

	    @Override
	    public List<StockOption> getIdeesByUserId(Integer i) {
	        List<StockOption> ideas = new ArrayList<>();
	        TypedQuery  query = em.createQuery("SELECT x FROM stockoption as x where x.trader_id=?",StockOption.class);
	       	        	        List<StockOption> resultList = query.getResultList();
				        return ideas;
	    }
	    public List<Object[]> StrikePriceOfThisMonthStockOption()
		{
			Query query = em
					.createQuery("SELECT  SUM(c.strikePriceStock), c.expirationDateStock FROM StockOption c WHERE c.typeoptionStock like '%call%' group by c.expirationDateStock ");
			List<Object[]> l = query.getResultList();
			return l;
		}
	
	  /*  @Override
	    public List<StockOption> findOptionById(float nbShares){
	    	 TypedQuery<StockOption> query=em.createQuery("SELECT e FROM" + "Stockoption e WHERE e.nbShares =:param",StockOption.class);
	    	query.setParameter("param", nbShares);
	    	  return(query.getResultList());
	    }*/
	
	    @Override
	    public float calculValueShares(int id) {
	    
	    	StockOption so = em.find(StockOption.class, id);
	    	System.out.println("StockOption  " + so);
	    	Stock s = so.getStock();
	    	System.out.println("*****************************************************************Stock  " + s);
	    	float y =((so.getNbShares())*(s.getValueTotal()))/(s.getNbStockTotal());	
	        return y ;
	    }
	    
	    
	    @Override
		public float calculValueShares(float a,float b,float c) {

			//StockOption so = em.find(StockOption.class, id);
			//System.out.println("StockOption  " + so);
			//Stock s = so.getStock();
			//System.out.println("Stock  " + s);
			//float y = ((so.getNbShares()) * (s.getValueTotal())) / (s.getNbStockTotal());
			float y = (a * b) / c;

			return y;
		}

		public float StockOptionPutCall(int id) {
			float intrinsicValue = 0;
			StockOption o = em.find(StockOption.class, id);

			o.getTypeOptionStock();
			if (o.getTypeOptionStock() == o.getTypeOptionStock().put) {

				// Call Option Intrinsic Value = Underlying Stock's Current Price –
				// Call Strike Price

				intrinsicValue = (float) ((o.getStrikePriceStock() - o.getCurrentPriceStock()));
			} else {
				// Put Option Intrinsic Value = Put Strike Price – Underlying
				// Stock's Current Price

				if (o.getTypeOptionStock() == o.getTypeOptionStock().call) {
					intrinsicValue = (float) ((o.getCurrentPriceStock() - o.getStrikePriceStock()));

				}

			}
			return intrinsicValue;
		}

		/*
		 * The time value of options is the amount by which the price of any option
		 * exceeds the intrinsic value. It is directly related to how much time an
		 * option has until it expires as well as the volatility of the stock. The
		 * formula for calculating the time value of an option is:
		 */
		public float StockOptionTimeValue(int id) {
			float TimeValue = 0;
			StockOption o = em.find(StockOption.class, id);
			TimeValue = (o.getCurrentPriceStock() - StockOptionPutCall(o.getId()));
			return TimeValue;

		}

		/* valuing futures contracts */
		// The futures price of an asset without payouts is the future value of the
		// current price of the assset.

		public float futuresPrice(int id) {
			StockOption o = em.find(StockOption.class, id);

			float fp, fp1;
			fp1 = (float) Math.exp((o.getRiskStock()) * (o.getTimeStock()));
			fp = (fp1 * o.getCurrentPriceStock());
			return fp;
		}

		// OptionPremium = time value + intrinsec value
		@Override
		public float OptionPriceFormula(int id) {
			StockOption o = em.find(StockOption.class, id);
			float OptionPremium = 0;
			o.getTypeOptionStock();
			if (o.getTypeOptionStock() == o.getTypeOptionStock().call) {

				OptionPremium = (StockOptionTimeValue(o.getId())) + (StockOptionPutCall(o.getId()));
			} else {	

				if (o.getTypeOptionStock() == o.getTypeOptionStock().put) {
					OptionPremium = (StockOptionTimeValue(o.getId())) + (StockOptionPutCall(o.getId()));
				}
			}
			return OptionPremium;
		}

		@Override
		public String DeepInOutAtTheMoney(int id) {
			StockOption o = em.find(StockOption.class, id);
			String DeepInOutAtTheMoney = "";
			o.getTypeOptionStock();
			if (o.getTypeOptionStock() == o.getTypeOptionStock().call) {
				
				 if (o.getCurrentPriceStock() < o.getStrikePriceStock()) {
						DeepInOutAtTheMoney = "Out of the money Call Option";
				
				 }else if (o.getCurrentPriceStock() == o.getStrikePriceStock()) {//current price=strike price
						DeepInOutAtTheMoney = "At the money call option";
					
					}else if((o.getCurrentPriceStock() > o.getStrikePriceStock())){
						// current market price of the stock is above the strike price of the call.
						DeepInOutAtTheMoney = "In the Money Call Option";
						}
			}
			else { if (o.getTypeOptionStock() == o.getTypeOptionStock().put) {
			
				if (o.getCurrentPriceStock() > o.getStrikePriceStock()) {
					DeepInOutAtTheMoney = "Out of the money put Option";
			
			 }else if (o.getCurrentPriceStock() == o.getStrikePriceStock()) {//current price=strike price
					DeepInOutAtTheMoney = "At the money put option";
				
				}else if((o.getCurrentPriceStock() < o.getStrikePriceStock())){
					// current market price of the stock is above the strike price of the call.
					DeepInOutAtTheMoney = "In the Money put Option";
					
					}
				
							}
				}
			return DeepInOutAtTheMoney;
		}

		@Override
		public float PriceOfALookbackOption(float S, float Smin, float r, float q, float sigma, float time) {
	float loockBackPrice=0;
	//float sigma=;
			if(r==q) return 0;
			float sigmaSqt=sigma*sigma;
			float timeSqrt= (float) Math.sqrt(time);
			float a1=(float) ((Math.log(S/Smin))+(r-q+sigmaSqt/2.0)*time)/(sigma*timeSqrt);
			float a2=a1-(sigma*timeSqrt) ;
			float a3=(float) ((Math.log(S/Smin))+(-r+q+sigmaSqt/2.0)*time)/(sigma*timeSqrt);
			float Y1=(float) (2.0*(r-q-sigmaSqt/2.0)*(Math.log(S/Smin)/sigmaSqt));
			loockBackPrice=(float) (S*Math.exp(-q*time)*((1/Math.sqrt(2*Math.PI))*(Math.exp((-a1)/2)))-S*Math.exp(-q*time)*(sigmaSqt/(2.0*(r-q)))*((1/Math.sqrt(2*Math.PI))*(Math.exp((-a1)/2)))-Smin*Math.exp(-r*time)*((1/Math.sqrt(2*Math.PI))*(Math.exp((-a2)/2))-(sigmaSqt/(2*(r-q)))*Math.exp(Y1)*(1/Math.sqrt(2*Math.PI))*(Math.exp((a3)/2))));
			return Math.abs(loockBackPrice);
				
		}

		@Override
		public float WarrantPriceAdjustedBS(int id) {
			StockOption o = em.find(StockOption.class, id);
			float NbWarrant=2;
			 float epsilon=0.00001f;
			float timesqrt=(float) Math.sqrt(o.getTimeStock());
			float w=(o.getNbShares()/(o.getNbShares()+NbWarrant))*PriceOfALookbackOption(10000,1000,0.4f,15,20,7);
			float g=w-(o.getNbShares()/(o.getNbShares()+NbWarrant))*PriceOfALookbackOption(o.getCurrentPriceStock()+((o.getNbShares()/NbWarrant)),1000,0.4f,15,20,7);
			while (g>epsilon) {
				float  d1 = (float) (Math.log(o.getCurrentPriceStock()+(NbWarrant+o.getNbShares()) /o.getStrikePriceStock()) + (o.getRiskStock()  * o.getTimeStock()) / (o.getVolatilityStock() * timesqrt)+0.5*o.getVolatilityStock()*timesqrt);
				float gprime=(float) (1-(NbWarrant/o.getNbShares())*((1/Math.sqrt(2*Math.PI))*(Math.exp((-d1)/2))));
				w=w-(g*gprime);
				g=w-(o.getNbShares()/(o.getNbShares()+NbWarrant))*PriceOfALookbackOption(o.getCurrentPriceStock()+((o.getNbShares()/NbWarrant)),1000,0.4f,15,20,7);
				
			}
			return Math.abs(w);
		}

		@Override
		public void UpdateOptionStatus(int id, StateOptionStock status) {
			StockOption stockOption = em.find(StockOption.class, id);
			stockOption.setStateOptionStock(status);		
		}

	
		
}
