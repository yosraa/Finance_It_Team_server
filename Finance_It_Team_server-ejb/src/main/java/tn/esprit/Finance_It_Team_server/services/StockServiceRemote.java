package tn.esprit.Finance_It_Team_server.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;

@Stateless

public class StockServiceRemote implements IStockService {
	@PersistenceContext(unitName="Finance_It_Team_server-ejb")
	EntityManager em;
	
	
	@Override
	public int addStock(Stock stock) {
		em.persist(stock);
		return stock.getId();
	}

	@Override
	public int deleteStock(int id) {
		Stock st = em.find(Stock.class,id);
		em.remove(st);
		return id;
	}

	@Override
	public void updateStock(Stock stock) {
		em.merge(stock);			
		
	}

	@Override
	public float showStock(int id) {
		Stock st = em.find(Stock.class,id);
		return st.getNbStockTotal();
	}

	@Override
	public List<Stock> getAllStock() {
		TypedQuery<Stock> query = em.createQuery("Select st from " + Stock.class.getName() + " st ",
				Stock.class);

		return query.getResultList();
	}

	@Override
	public void affectAStockToAStockOption(int stockId, int stockOptionId) {
		Stock t= em.find(Stock.class,stockId);
	StockOption st = em.find(StockOption.class,stockOptionId);
		st.setStock(t);
		
	}

	@Override
	public List<Stock> findAllStock() {
		TypedQuery  query = em.createQuery("SELECT x FROM stock as x",Stock.class);
		List<Stock> resultList = query.getResultList();
		return  resultList;
	}

}
