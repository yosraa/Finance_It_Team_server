package tn.esprit.Finance_It_Team_server.services;

import java.util.ArrayList;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import java.util.List;

@Stateless
@LocalBean
public class Volatility implements VolatilityRemote {

	
	/**
	 * 
	 */
	public double volatilty_value(ArrayList<Float> allClose) {

		ArrayList<Float> newCal = new ArrayList<>();

		

		Double somme = 0.0d;

		for (int i = 0; i < allClose.size() - 1; i++) {
			newCal.add((allClose.get(i + 1) / allClose.get(i)) - 1);
		}
		
		for (Float close : newCal) {
			somme = somme + close;
		}
		
	
		Double moyenne = somme / newCal.size();
		Double cumul = 0.0d;
		for (Float close : newCal) {
			cumul += Math.pow(close - moyenne, 2);
		}
		return Math.pow(cumul / (allClose.size() - 1), 0.5);

	}

}
