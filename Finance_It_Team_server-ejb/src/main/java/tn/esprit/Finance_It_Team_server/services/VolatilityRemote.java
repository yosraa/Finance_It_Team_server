package tn.esprit.Finance_It_Team_server.services;

import java.util.ArrayList;

import javax.ejb.Remote;

@Remote
public interface VolatilityRemote {
	public double volatilty_value(ArrayList<Float> allClose);

}
