package tn.esprit.Finance_It_Team_server.mBeans;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.SendMail;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;

@ViewScoped
// @SessionScoped
// @ApplicationScoped
@ManagedBean
public class AdminStockOptionBean {
	private float strikePriceStock;
	private float currentPriceStock;
	private TypeOptionStock typeOptionStock; // enum hedhi lezemha classe ennum
	private Date expirationDateStock; // nzid nchouf le type mta3 l date
	private float volatilityStock;
	private float riskStock;
	private float timeStock;
	private StateOptionStock stateOptionStock; // enum hedhi lezemha classe
												// ennum
	private Date dateOptionBegin;
	private Date dateOptionEnd;
	private float nbShares;
	private List<StockOption> stockOptions;
	private Integer stockOptionIdToBeUpdated;
	private String value;
	@EJB
	StockOptionServiceRemote stockOptionService;

	@PostConstruct // elle s'execute juste apres (l instantiation du bean et l
					// injection de dependance(le responsable aliha @EJB w le
					// composant logiciel qui le fait seveur d application "ejb
					// container"))
	public void init() {
		expirationDateStock = new Date();
		dateOptionBegin = new Date();
		dateOptionEnd = new Date();
		stockOptions = stockOptionService.getAllStockOption();

	}

	String navigateTo = "null";

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String modifier(StockOption stockOption) {
	

		this.setStockOptionIdToBeUpdated(stockOption.getId());


		this.setStrikePriceStock(stockOption.getStrikePriceStock());
		this.setCurrentPriceStock(stockOption.getCurrentPriceStock());
		this.setTypeOptionStock(stockOption.getTypeOptionStock());

		this.setExpirationDateStock(stockOption.getExpirationDateStock());
		this.setVolatilityStock(stockOption.getVolatilityStock());
		this.setRiskStock(stockOption.getRiskStock());
		this.setTimeStock(stockOption.getTimeStock());

		this.setStateOptionStock(stockOption.getStateOptionStock());
		this.setDateOptionBegin(stockOption.getDateOptionBegin());
		this.setDateOptionEnd(stockOption.getDateOptionEnd());
		this.setNbShares(stockOption.getNbShares());
		return "UpdateStockOption?faces-redirect=true";

	}

	public String supprimer(int optionId) {
		stockOptionService.findStockOptioneById(optionId);
		stockOptionService.deleteStockOption(optionId);
		return "ListStockOption?faces-redirect=true";

	}

	public String mettreAjourStockOption() {
		stockOptionService.updateStockOption(new StockOption(stockOptionIdToBeUpdated, strikePriceStock,
				currentPriceStock, typeOptionStock, expirationDateStock, volatilityStock, riskStock, timeStock,
				stateOptionStock, dateOptionBegin, dateOptionEnd, nbShares));
		return "ListStockOption?faces-redirect=true";

	}

	public void validate(StockOption stockOption) {

		this.setStockOptionIdToBeUpdated(stockOption.getId());
		if (stockOption.getStateOptionStock() == StateOptionStock.no) {
			stockOption.setStateOptionStock(StateOptionStock.yes);
		} else if (stockOption.getStateOptionStock() == StateOptionStock.yes) {
			stockOption.setStateOptionStock(StateOptionStock.no);
		}

		stockOptionService.updateStockOption(stockOption);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		 String destination ="myriammalek.jemmali@esprit.tn";
	        String sujet="INTELLIXX";
	        String msg="Your Option is accepted at: "+"  " +dtf.format(now)+" the option will start in:" +getDateOptionBegin()+"   and it will exipire in: "+getDateOptionEnd();
	        String login="investinvest02@gmail.com";
	        String m2p="investinvest0";
	        SendMail.send(destination, sujet, msg, login, m2p);
		

	}

	public float getStrikePriceStock() {
		return strikePriceStock;
	}

	public void setStrikePriceStock(float strikePriceStock) {
		this.strikePriceStock = strikePriceStock;
	}

	public float getCurrentPriceStock() {
		return currentPriceStock;
	}

	public void setCurrentPriceStock(float currentPriceStock) {
		this.currentPriceStock = currentPriceStock;
	}

	public TypeOptionStock getTypeOptionStock() {
		return typeOptionStock;
	}

	public void setTypeOptionStock(TypeOptionStock typeOptionStock) {
		this.typeOptionStock = typeOptionStock;
	}

	public Date getExpirationDateStock() {
		return expirationDateStock;
	}

	public void setExpirationDateStock(Date expirationDateStock) {
		this.expirationDateStock = expirationDateStock;
	}

	public float getVolatilityStock() {
		return volatilityStock;
	}

	public void setVolatilityStock(float volatilityStock) {
		this.volatilityStock = volatilityStock;
	}

	public float getRiskStock() {
		return riskStock;
	}

	public void setRiskStock(float riskStock) {
		this.riskStock = riskStock;
	}

	public float getTimeStock() {
		return timeStock;
	}

	public void setTimeStock(float timeStock) {
		this.timeStock = timeStock;
	}

	public StateOptionStock getStateOptionStock() {
		return stateOptionStock;
	}

	public void setStateOptionStock(StateOptionStock stateOptionStock) {
		this.stateOptionStock = stateOptionStock;
	}

	public Date getDateOptionBegin() {
		return dateOptionBegin;
	}

	public void setDateOptionBegin(Date dateOptionBegin) {
		this.dateOptionBegin = dateOptionBegin;
	}

	public Date getDateOptionEnd() {
		return dateOptionEnd;
	}

	public void setDateOptionEnd(Date dateOptionEnd) {
		this.dateOptionEnd = dateOptionEnd;
	}

	public float getNbShares() {
		return nbShares;
	}

	public void setNbShares(float nbShares) {
		this.nbShares = nbShares;
	}

	public List<StockOption> getStockOptions() {
		return stockOptions;
	}

	public void setStockOptions(List<StockOption> stockOptions) {
		this.stockOptions = stockOptions;
	}

	public StockOptionServiceRemote getStockOptionService() {
		return stockOptionService;
	}

	public void setStockOptionService(StockOptionServiceRemote stockOptionService) {
		this.stockOptionService = stockOptionService;
	}

	public Integer getStockOptionIdToBeUpdated() {
		return stockOptionIdToBeUpdated;
	}

	public void setStockOptionIdToBeUpdated(Integer stockOptionIdToBeUpdated) {
		this.stockOptionIdToBeUpdated = stockOptionIdToBeUpdated;
	}

	
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

}
