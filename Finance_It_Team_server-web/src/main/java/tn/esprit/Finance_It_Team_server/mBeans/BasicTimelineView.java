package tn.esprit.Finance_It_Team_server.mBeans;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
 
@ManagedBean(name="basicTimelineView")
@ViewScoped
public class BasicTimelineView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TimelineModel model;
 
    private boolean selectable = true;
    private boolean zoomable = true;
    private boolean moveable = true;
    private boolean stackEvents = true;
    private String eventStyle = "box";
    private boolean axisOnTop;
    private boolean showCurrentTime = true;
    private boolean showNavigation = false;
 
    @PostConstruct
    protected void initialize() {
		
model = new TimelineModel();
		Calendar cal = Calendar.getInstance();
		cal.set(2018, Calendar.APRIL, Calendar.THURSDAY, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND);
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy|hh:mm:ssaa");
		LogReader.getLogging(SystemLog.logFile).forEach((k, v) -> {
			try {
				System.out.println(formatter.parse(k));
			//	model.add(new TimelineEvent("lol",cal.getTime()));
				model.add(new TimelineEvent(v, formatter.parse(k)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});
    }
 
    public void onSelect(TimelineSelectEvent e) {
        TimelineEvent timelineEvent = e.getTimelineEvent();
 
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected event:", timelineEvent.getData().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public TimelineModel getModel() {
        return model;
    }
 
    public boolean isSelectable() {
        return selectable;
    }
 
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }
 
    public boolean isZoomable() {
        return zoomable;
    }
 
    public void setZoomable(boolean zoomable) {
        this.zoomable = zoomable;
    }
 
    public boolean isMoveable() {
        return moveable;
    }
 
    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }
 
    public boolean isStackEvents() {
        return stackEvents;
    }
 
    public void setStackEvents(boolean stackEvents) {
        this.stackEvents = stackEvents;
    }
 
    public String getEventStyle() {
        return eventStyle;
    }
 
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }
 
    public boolean isAxisOnTop() {
        return axisOnTop;
    }
 
    public void setAxisOnTop(boolean axisOnTop) {
        this.axisOnTop = axisOnTop;
    }
 
    public boolean isShowCurrentTime() {
        return showCurrentTime;
    }
 
    public void setShowCurrentTime(boolean showCurrentTime) {
        this.showCurrentTime = showCurrentTime;
    }
 
    public boolean isShowNavigation() {
        return showNavigation;
    }
 
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }
}