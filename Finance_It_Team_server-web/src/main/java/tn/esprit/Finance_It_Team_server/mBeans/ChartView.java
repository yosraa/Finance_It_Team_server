package tn.esprit.Finance_It_Team_server.mBeans;
 
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.PieChartModel;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.TypeCurrency;
import tn.esprit.Finance_It_Team_server.services.CurrencyAccountRemote;

import org.primefaces.model.chart.ChartSeries;
 
@ManagedBean

public class ChartView implements Serializable {
 
    private BarChartModel barModel;
	private float amount;

	@ManagedProperty(value="#{currencyAccountBean}")
	private CurrencyAccountBean currencyAccountBean;
	
 
	@EJB
	CurrencyAccountRemote currencyAccountRemote;
    @PostConstruct
  
    public void init() {
        createBarModels();
    }
 
    public BarChartModel getBarModel() {
        return barModel;
    }
     
   
 
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Currency Acount");
        boys.set(currencyAccountBean.getC().getTypecurrency(),currencyAccountBean.getC().getAmount());
        boys.set("Dollar", 350);
        boys.set("Pound", 750);
        boys.set("Yen", 860);
 
       
        model.addSeries(boys);
         
        return model;
    }
     
    private void createBarModels() {
        createBarModel();
    }
     
    private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Bar Chart");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Gender");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Births");
        yAxis.setMin(0);
        yAxis.setMax(1000);
    }

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public CurrencyAccountBean getCurrencyAccountBean() {
		return currencyAccountBean;
	}

	public void setCurrencyAccountBean(CurrencyAccountBean currencyAccountBean) {
		this.currencyAccountBean = currencyAccountBean;
	}

	public CurrencyAccountRemote getCurrencyAccountRemote() {
		return currencyAccountRemote;
	}

	public void setCurrencyAccountRemote(CurrencyAccountRemote currencyAccountRemote) {
		this.currencyAccountRemote = currencyAccountRemote;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}
     
    
}
