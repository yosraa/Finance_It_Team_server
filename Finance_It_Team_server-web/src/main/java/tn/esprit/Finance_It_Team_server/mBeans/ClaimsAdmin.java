package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.ServiceReclamation;
import tn.esprit.Finance_It_Team_server.services.ServiceUser;

@ManagedBean
@ViewScoped
public class ClaimsAdmin {

	private List<Reclamation> listReclamation;
	@EJB
	ServiceReclamation serviceReclamation;
	
	
	public void doViewReclamation(Reclamation reclamation) {
		serviceReclamation.modifyState(reclamation.getId());
		
	}	

	public List<Reclamation> getListReclamation() {
		return serviceReclamation.listerReclamations();
	}
	public void setListReclamation(List<Reclamation> listReclamation) {
		this.listReclamation = listReclamation;
	}

}
