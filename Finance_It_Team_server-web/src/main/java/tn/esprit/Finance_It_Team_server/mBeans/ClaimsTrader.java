package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.ServiceReclamation;
import tn.esprit.Finance_It_Team_server.services.ServiceUser;

@ManagedBean
@ViewScoped
public class ClaimsTrader {
	private String description;
	private List<Reclamation> listReclamation;
	@EJB
	ServiceReclamation serviceReclamation;
	
	
	public void doSendReclamation(Integer idTrader) {
		System.out.println("1");
		Reclamation reclamation= new Reclamation();
		System.out.println("2");
		reclamation.setDescription(description);
		System.out.println("3");
		System.out.println(description);

		reclamation.setTraderId(idTrader);
		System.out.println("4");

		serviceReclamation.ajouterReclamation(reclamation);
		System.out.println("5");

	}	
	public List<Reclamation> listReclamation(Integer idTrader) {
		return serviceReclamation.listerReclamationParTrader(idTrader);
	}
	public List<Reclamation> getListReclamation(Integer idTrader) {
		return serviceReclamation.listerReclamationParTrader(idTrader);
	}
	public void setListReclamation(List<Reclamation> listReclamation) {
		this.listReclamation = listReclamation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
