package tn.esprit.Finance_It_Team_server.mBeans;
import tn.esprit.Finance_It_Team_server.mBeans.SystemLog;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import tn.esprit.Finance_It_Team_server.services.MailService;


@Named
@RequestScoped
public class ClientBean {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatusMessage() {
        return statusMessage;
    }
    
    public String send() {

    	statusMessage = "Message Sent";
       MailService.send(recipient, subject, message, USER, PASSWORD);
        return "index";  // redisplay page with status message
        

    }
    

      private static final String USER = "investinvest02@gmail.com";     // Must be valid user in d.umn.edu domain, e.g. "smit0012"
      private static final String PASSWORD = "investinvest0"; // Must be valid password for smit0012
    private String recipient;
    private String subject;
    private String message;
    private String statusMessage = "";

    
}