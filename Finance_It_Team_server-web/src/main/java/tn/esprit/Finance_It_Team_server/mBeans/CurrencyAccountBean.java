package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;

import tn.esprit.Finance_It_Team_server.entities.TypeCurrency;
import tn.esprit.Finance_It_Team_server.services.CurrencyAccountRemote;


@ManagedBean
@SessionScoped
public class CurrencyAccountBean {
	
	private int id;
	private float amount;
	private float sumIncome;
	private float sumExpenses;
	private TypeCurrency typecurrency;
	private List<CurrencyAccount> curracc;

	
	private CurrencyAccount c;


	@EJB
	CurrencyAccountRemote currencyaccountremote  ;
	
	
	
	    
	

	public void addCurrencyAcc()
	{
		CurrencyAccount currencyAccount= new  CurrencyAccount(amount,  sumIncome,  sumExpenses,  typecurrency);
		currencyaccountremote.addCurrecnyAccount(currencyAccount, 9);
		c=currencyAccount;
	}
	
	public void supprimerCurrencyAcc (Integer id) {
		
		currencyaccountremote.deleteCurrecnyAccount(id);
		
	}
	
	public void modifierCurrencyAcc(CurrencyAccount acc) {
		this.setSumExpenses(acc.getSumExpenses());
		this.setSumIncome(acc.getSumIncome());
		this.setAmount(acc.getAmount());
		this.setTypecurrency(acc.getTypecurrency());
		
		this.setId(acc.getId());
	}
	
	public void mettreAjourAccount() {
		System.out.println(id);
		currencyaccountremote.updateCurrecnyAccount(new CurrencyAccount(id,amount,sumExpenses,sumIncome,typecurrency));

	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public float getAmount() {
		return amount;
	}


	public void setAmount(float amount) {
		this.amount = amount;
	}


	public float getSumIncome() {
		return sumIncome;
	}


	public void setSumIncome(float sumIncome) {
		this.sumIncome = sumIncome;
	}


	public float getSumExpenses() {
		return sumExpenses;
	}


	public void setSumExpenses(float sumExpenses) {
		this.sumExpenses = sumExpenses;
	}


	public TypeCurrency getTypecurrency() {
		return typecurrency;
	}


	public void setTypecurrency(TypeCurrency typecurrency) {
		this.typecurrency = typecurrency;
	}

	
	public List<CurrencyAccount> getCurracc() {
		curracc = currencyaccountremote.getAllCurrencyAccount();
		return curracc;
	}


	public void setCurracc(List<CurrencyAccount> curracc) {
		this.curracc = curracc;
	}


	public CurrencyAccountRemote getCurrencyaccountremote() {
		return currencyaccountremote;
	}


	public void setCurrencyaccountremote(CurrencyAccountRemote currencyaccountremote) {
		this.currencyaccountremote = currencyaccountremote;
	}

	public CurrencyAccount getC() {
		return c;
	}

	public void setC(CurrencyAccount c) {
		this.c = c;
	}

	 
}
