package tn.esprit.Finance_It_Team_server.mBeans;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;

import tn.esprit.Finance_It_Team_server.services.CalculService;

@javax.faces.bean.ManagedBean
@SessionScoped
public class CurrencyConverterBean {
	String fromcurrency;
	String tocurrency;
	double montantfrom;
	double montantto;

	@EJB
	CalculService calculService;

	public String getFromcurrency() {
		return fromcurrency;
	}

	public void setFromcurrency(String fromcurrency) {
		this.fromcurrency = fromcurrency;
	}

	public double getMontantfrom() {
		return montantfrom;
	}

	public void setMontantfrom(double montantfrom) {
		this.montantfrom = montantfrom;
	}

	public double getMontantto() {
		return montantto;
	}

	public void setMontantto(double montantto) {
		this.montantto = montantto;
	}

	public String getTocurrency() {
		return tocurrency;
	}

	public void setTocurrency(String tocurrency) {
		this.tocurrency = tocurrency;
	}

	public CalculService getCalculService() {
		return calculService;
	}

	public void setCalculService(CalculService calculService) {
		this.calculService = calculService;
	}

	public double convertcurrency(String fromcurrency, String tocurrency, double montantfrom) {
		double val=calculService.currencyConvertion(fromcurrency, tocurrency);
		montantto=val*montantfrom;

		return montantto;
	}

}
