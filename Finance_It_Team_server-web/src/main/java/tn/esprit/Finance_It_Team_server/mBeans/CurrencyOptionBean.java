package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;
import tn.esprit.Finance_It_Team_server.services.OptionProductServiceRemote;

@ManagedBean
@SessionScoped
public class CurrencyOptionBean {
	private int id;
	private float strikePrice;
	private float currentPrice;
	private TypeOption typeoption;
	private Date expirationDate;
	private float volatility;
	private float risk;
	private float time;
	private float spotExchangeRate;
	private UnitExchange unitEchange;
	private StateOption stateOption;
	private double premium;
	private int Archive;
	private List<CurrencyOption> currop;
	private List<CurrencyOption> curropArchive;
	private List<CurrencyOption> currOwnOp;
	private List<CurrencyOption> allCurrOp;
	private List<CurrencyOption> idcurrachat;
	private long riskcount;
	private Trader trader;
	private CurrencyOption cu;

	public CurrencyOption getCu() {
		return cu;
	}

	public void setCu(CurrencyOption cu) {
		this.cu = cu;
	}

	public Trader getTrader() {
		return trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	public long getRiskcount() {
		optionProductServiceRemote.countRisk();
		return riskcount;
	}

	public long riskkk() {
		return optionProductServiceRemote.countRisk();
	}

	public long riskkkMax() {
		return optionProductServiceRemote.countRiskMax();
	}

	public long riskkkMin() {
		return optionProductServiceRemote.countRiskMin();
	}

	public float highrisk() {
		float rm = (long) ((float) riskkkMax());
		float r = (long) ((float) riskkk());
		return (rm / r) * 100;
	}

	public float lowrisk() {
		float rmi = (long) ((float) riskkkMin());
		float r = (long) ((float) riskkk());
		return (rmi / r) * 100;
	}

	public long caluculUnitExTotal() {
		return optionProductServiceRemote.calculerUnit();
	}

	public float pourcent() {
		float ct = (long) ((float) caluculUnitExTotal());
		float c = (long) ((float) caluculUnitEx());
		return (c / ct) * 100;
	}

	public float pourcent1() {
		float ct = (long) ((float) caluculUnitExTotal());
		float c = (long) ((float) calucul1());
		return (c / ct) * 100;
	}

	public float pourcent2() {
		float ct1 = (long) ((float) caluculUnitExTotal());
		float c1 = (long) ((float) calucul2());
		return (c1 / ct1) * 100;
	}

	public float pourcent3() {
		float ct1 = (long) ((float) caluculUnitExTotal());
		float c1 = (long) ((float) calucul3());
		return (c1 / ct1) * 100;
	}

	public float pourcent4() {
		float ct1 = (long) ((float) caluculUnitExTotal());
		float c1 = (long) ((float) calucul4());
		return (c1 / ct1) * 100;
	}

	public long caluculUnitEx() {
		return optionProductServiceRemote.calculerUnitCAD_EUR();
	}

	public long calucul1() {
		return optionProductServiceRemote.calculerUnitEur_TND();
	}

	public long calucul2() {
		return optionProductServiceRemote.calculerUnitJPY_EUR();
	}

	public long calucul3() {
		return optionProductServiceRemote.calculerUnitUSD_EUR();
	}

	public long calucul4() {
		return optionProductServiceRemote.calculerUnitUSD_TDN();
	}

	public void setRiskcount(long riskcount) {
		this.riskcount = riskcount;
	}

	public List<CurrencyOption> getAllCurrOp() {
		allCurrOp = optionProductServiceRemote.getAllCurrencyOption1();
		return allCurrOp;
	}

	public void setAllCurrOp(List<CurrencyOption> allCurrOp) {
		this.allCurrOp = allCurrOp;
	}

	public List<CurrencyOption> getCurrOwnOp() {
		currOwnOp = optionProductServiceRemote.getAllCurrencyOptionIdTrader(id);
		return currOwnOp;
	}

	public void setCurrOwnOp(List<CurrencyOption> currOwnOp) {
		this.currOwnOp = currOwnOp;
	}

	private float gainOrLoss10;

	@EJB
	OptionProductServiceRemote optionProductServiceRemote;

	public String addCurrncyOp(int id) {
		CurrencyOption currencyOption = new CurrencyOption(currentPrice, strikePrice, risk, volatility, time,
				typeoption, expirationDate, unitEchange, spotExchangeRate, premium);

		currencyOption.setStateOption(StateOption.unavailable);
		optionProductServiceRemote.addOptionProduct(currencyOption, id);

		return "/pages/OwnCurrencyOption?faces-redirect=true";

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getStrikePrice() {
		return strikePrice;
	}

	public void setStrikePrice(float strikePrice) {
		this.strikePrice = strikePrice;
	}

	public float getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(float currentPrice) {
		this.currentPrice = currentPrice;
	}

	public TypeOption getTypeoption() {
		return typeoption;
	}

	public void setTypeoption(TypeOption typeoption) {
		this.typeoption = typeoption;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public float getVolatility() {
		return volatility;
	}

	public void setVolatility(float volatility) {
		this.volatility = volatility;
	}

	public float getRisk() {
		return risk;
	}

	public void setRisk(float risk) {
		this.risk = risk;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public float getSpotExchangeRate() {
		return spotExchangeRate;
	}

	public void setSpotExchangeRate(float spotExchangeRate) {
		this.spotExchangeRate = spotExchangeRate;
	}

	public UnitExchange getUnitEchange() {
		return unitEchange;
	}

	public void setUnitEchange(UnitExchange unitEchange) {
		this.unitEchange = unitEchange;
	}

	public StateOption getStateOption() {
		return stateOption;
	}

	public void setStateOption(StateOption stateOption) {
		this.stateOption = stateOption;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public int getArchive() {
		return Archive;
	}

	public void setArchive(int archive) {
		Archive = archive;
	}

	public List<CurrencyOption> getCurrop() {
		currop = optionProductServiceRemote.getAllCurrencyOptionNotIdTrader(27);
		return currop;
	}

	public void setCurrop(List<CurrencyOption> currop) {
		this.currop = currop;
	}

	public float getGainOrLoss10(int id) {
		gainOrLoss10 = optionProductServiceRemote.gainOrLossOfTraderCallAndPut(id);
		this.setGainOrLoss10(gainOrLoss10);

		return gainOrLoss10;

	}

	public void setGainOrLoss10(float gainOrLoss10) {
		this.gainOrLoss10 = gainOrLoss10;

	}

	public List<CurrencyOption> getCurropArchive() {

		curropArchive = optionProductServiceRemote.getCurrencyOptionById1();
		return curropArchive;

	}

	public List<CurrencyOption> redirection(UnitExchange unitEchange) {
		System.out.println("hhhhhhhhhhhh");
		System.out.println(optionProductServiceRemote.findOptionByUnitExchange(unitEchange));
		return optionProductServiceRemote.findOptionByUnitExchange(unitEchange);

	}

	public String search() {
		return null;
	}

	public String redi() {

		return "/pages/Search?faces-redirect=true";
	}

	public void modif(CurrencyOption curroption) {

		this.setId(curroption.getId());
		System.out.println(id);
		if (curroption.getArchive() == 0) {
			curroption.setArchive(1);
		}
		optionProductServiceRemote.updateOption(curroption);

	}

	public void validate(CurrencyOption curruencyoption) {

		this.setId(curruencyoption.getId());
		System.out.println(id);
		if (curruencyoption.getStateOption() == StateOption.unavailable) {
			curruencyoption.setStateOption(StateOption.available);
		}
		optionProductServiceRemote.updateOption(curruencyoption);

	}

	public String ajoutAchatCUrrencyOp(int x) {

		idcurrachat = optionProductServiceRemote.getoptionById(x);

		return "/pages/TradingForex?faces-redirect=true";
	}

	public List<CurrencyOption> getIdcurrachat() {

		return idcurrachat;
	}

	public void setIdcurrachat(List<CurrencyOption> idcurrachat) {
		this.idcurrachat = idcurrachat;
	}

	public void setCurropArchive(List<CurrencyOption> curropArchive) {
		this.curropArchive = curropArchive;
	}

	public List<CurrencyOption> Affichercurrency(int id) {

		return optionProductServiceRemote.getAllCurrencyOptionIdTrader(id);

	}

	public List<CurrencyOption> AfficherNotcurrency(int id) {

		return optionProductServiceRemote.getAllCurrencyOptionNotIdTrader(id);

	}

	public void supprimer(int id) {
		optionProductServiceRemote.deleteOptionProduct(id);
	}

}
