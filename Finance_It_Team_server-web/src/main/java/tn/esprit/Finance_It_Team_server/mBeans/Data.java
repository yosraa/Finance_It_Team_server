package tn.esprit.Finance_It_Team_server.mBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.TypeCurrency;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;



@ManagedBean
@ApplicationScoped
public class Data {
	public StateOption[] getStateOptions(){
		return  StateOption.values();
	}
	public TypeOption[] getTypeOptions(){
		return  TypeOption.values();
	}

	public UnitExchange[] getUnitExchanges(){
		return  UnitExchange.values();
	}
	public TypeCurrency[] getTypeCurrencyAccount(){
		return  TypeCurrency.values();
	}


}
