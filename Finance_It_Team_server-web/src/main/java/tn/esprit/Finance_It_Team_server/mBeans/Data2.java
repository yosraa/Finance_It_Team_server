package tn.esprit.Finance_It_Team_server.mBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;



@ManagedBean
@ApplicationScoped
public class Data2 {
	public StateOptionStock[] getStateOptionsOptionStocks(){
		return  StateOptionStock.values();
	}
	public TypeOptionStock[] getTypeOptionsOptionStocks(){
		return  TypeOptionStock.values();
	}



}
