package tn.esprit.Finance_It_Team_server.mBeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.Finance_It_Team_server.services.Greeks;

@ManagedBean
@SessionScoped
public class GreeksBean {
	private double deltacall;
	private double deltaput;
	private double vegacall;
	private double vegaput;
	private double rhocall;
	private double rhoput;
	private double gammacall;
	private double gammaput;
	@EJB
	Greeks greeksservice;
	
	
	OptionPriceBean optionbean=new OptionPriceBean();

	public double getDeltacall() {
		return deltacall;
	}

	public void setDeltacall(double deltacall) {
		this.deltacall = deltacall;
	}

	public double getDeltaput() {
		return deltaput;
	}

	public void setDeltaput(double deltaput) {
		this.deltaput = deltaput;
	}

	public double getVegacall() {
		return vegacall;
	}

	public void setVegacall(double vegacall) {
		this.vegacall = vegacall;
	}

	public double getVegaput() {
		return vegaput;
	}

	public void setVegaput(double vegaput) {
		this.vegaput = vegaput;
	}

	public double getRhocall() {
		return rhocall;
	}

	public void setRhocall(double rhocall) {
		this.rhocall = rhocall;
	}

	public double getRhoput() {
		return rhoput;
	}

	public void setRhoput(double rhoput) {
		this.rhoput = rhoput;
	}

	public double getGammacall() {
		return gammacall;
	}

	public void setGammacall(double gammacall) {
		this.gammacall = gammacall;
	}

	public double getGammaput() {
		return gammaput;
	}

	public void setGammaput(double gammaput) {
		this.gammaput = gammaput;
	}

	public Greeks getGreeksservice() {
		return greeksservice;
	}

	public void setGreeksservice(Greeks greeksservice) {
		this.greeksservice = greeksservice;
	}

	public GreeksBean(double deltacall, double deltaput, double vegacall, double vegaput, double rhocall, double rhoput,
			double gammacall, double gammaput) {
		super();
		this.deltacall = deltacall;
		this.deltaput = deltaput;
		this.vegacall = vegacall;
		this.vegaput = vegaput;
		this.rhocall = rhocall;
		this.rhoput = rhoput;
		this.gammacall = gammacall;
		this.gammaput = gammaput;
	}
	public void setgreeksvalues(){
		double s=optionbean.getCurrentprice();
		double k=optionbean.getStrikeprice();
		double sigma=optionbean.getVolatility();
		double t=optionbean.getTime();
		double r=optionbean.getRisk();
		double q=optionbean.getDividendYield();
		
		deltacall=greeksservice.delta(s, k, r, sigma, t, q, "call");
		deltaput=greeksservice.delta(s, k, r, sigma, t, q, "put");
		vegacall=greeksservice.vega(s, k, r, sigma, t, q);
		vegaput=greeksservice.vega(s, k, r, sigma, t, q);
		rhocall=greeksservice.rho(s, k, r, sigma, t, "call");
		rhoput=greeksservice.rho(s, k, r, sigma, t, "put");
		gammacall=greeksservice.gamma(s, k, r, sigma, t, q);
		gammaput=gammacall;
	}
	
	

}
