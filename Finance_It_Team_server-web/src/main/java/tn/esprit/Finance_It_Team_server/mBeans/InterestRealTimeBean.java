package tn.esprit.Finance_It_Team_server.mBeans;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.services.CalculService;
import tn.esprit.Finance_It_Team_server.utils.Rates;



@ManagedBean
@ViewScoped
public class InterestRealTimeBean {
	
	@EJB
	 CalculService calculService;
	
	private List<String> listSymbols;
	
	private List<Rates> listRates;
	
	public List<String> getListSymbols() {
		return listSymbols;
	}

	public void setListSymbols(List<String> listSymbols) {
		this.listSymbols = listSymbols;
	}

	public List<Rates> getListRates() {
		return listRates;
	}

	public void setListRates(List<Rates> listRates) {
		this.listRates = listRates;
	}

	@PostConstruct
	public void init() {
	
		
	
		
		listSymbols = new ArrayList<>();
		listRates = new ArrayList<>();
		
		listSymbols.add("usd");
		listSymbols.add("eur");
		listSymbols.add("aud");
		listSymbols.add("cad");
		listSymbols.add("cny");
		listSymbols.add("inr");
		listSymbols.add("gbp");
		listSymbols.add("jpy");
		listSymbols.add("rub");
		listSymbols.add("tnd");
		/****/
		for(String s : listSymbols)
		{
			Rates rates = new Rates();
			rates.setSymbole("/ressources/img/"+s+".png");
			rates.setTaux(calculService.currencyConvertion(s));
			listRates.add(rates);
		}
	}
	
	

}
