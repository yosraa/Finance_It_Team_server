package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.AxisType;

import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;

import tn.esprit.Finance_It_Team_server.services.OptionProductServiceRemote;

@ManagedBean
public class LineChartY implements Serializable {

	private LineChartModel lineModel1;
	private LineChartModel lineModel2;

	@EJB
	OptionProductServiceRemote optionProductServiceRemote;

	public List<Object[]> strikePriceOfThisMonth() {
		return optionProductServiceRemote.StrikePriceOfThisMonth1();
	}

	public List<Object[]> PriceOfThisMonth() {
		return optionProductServiceRemote.CurrentPriceOfThisMonth1();
	}

	public List<Object[]> strikePriceOfThisMonthPut() {
		return optionProductServiceRemote.StrikePriceOfThisMonth2();
	}

	public List<Object[]> PriceOfThisMonthPut() {
		return optionProductServiceRemote.CurrentPriceOfThisMonth2();
	}

	@PostConstruct
	public void init() {
		lineModel1 = initCategoryModel();
		lineModel2 = initCategoryModel11();
	}

	public LineChartModel getLineModel1() {
		return lineModel1;
	}

	public LineChartModel getLineModel2() {
		return lineModel2;
	}

	private LineChartModel initCategoryModel() {

		LineChartModel model = new LineChartModel();
		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String today = dateFormat.format(Calendar.getInstance().getTime());
		int moisNow = Integer.parseInt((today.toString().substring(5, 7)));

		ChartSeries boys = new ChartSeries();
		ChartSeries girls = new ChartSeries();
		boys.setLabel("Call");

		model.setTitle("Price Per Month Of Call Option ");
		model.setZoom(true);
		model.getAxis(AxisType.Y).setLabel("Price");
		DateAxis axis = new DateAxis("Dates");
		axis.setTickAngle(-50);
		axis.setTickFormat("%b %#d, %y");
		axis.setMax("2018-05-30");
		model.getAxes().put(AxisType.X, axis);

		for (Object[] o : strikePriceOfThisMonth()) {

			Double price = (Double) o[0];

			Date date = (Date) o[1];

			String date1 = dateFormat.format(date.getTime());

			int mois1 = Integer.parseInt((date1.toString().substring(5, 7)));

			if (mois1 == moisNow) {

				boys.set(date1.toString(), price);

			}

		}
		for (Object[] o : PriceOfThisMonth()) {

			Double price1 = (Double) o[0];

			Date date1 = (Date) o[1];

			String date12 = dateFormat.format(date1.getTime());

			int mois1 = Integer.parseInt((date12.toString().substring(5, 7)));

			if (mois1 == moisNow) {

				girls.set(date12.toString(), price1);

			}

		}

		model.addSeries(boys);
		model.addSeries(girls);
		return model;
	}

	private LineChartModel initCategoryModel11() {

		LineChartModel model = new LineChartModel();
		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String today = dateFormat.format(Calendar.getInstance().getTime());
		int moisNow = Integer.parseInt((today.toString().substring(5, 7)));

		ChartSeries boys = new ChartSeries();
		ChartSeries girls = new ChartSeries();
		boys.setLabel("Call");

		model.setTitle("Price Per Month of Put Option ");
		model.setZoom(true);
		model.getAxis(AxisType.Y).setLabel("Price");
		DateAxis axis = new DateAxis("Dates");
		axis.setTickAngle(-50);
		axis.setTickFormat("%b %#d, %y");
		axis.setMax("2018-05-30");
		model.getAxes().put(AxisType.X, axis);

		for (Object[] o : strikePriceOfThisMonthPut()) {

			Double price = (Double) o[0];

			Date date = (Date) o[1];

			String date1 = dateFormat.format(date.getTime());

			int mois1 = Integer.parseInt((date1.toString().substring(5, 7)));

			if (mois1 == moisNow) {

				boys.set(date1.toString(), price);

			}

		}
		for (Object[] o : PriceOfThisMonthPut()) {

			Double price1 = (Double) o[0];

			Date date1 = (Date) o[1];

			String date12 = dateFormat.format(date1.getTime());

			int mois1 = Integer.parseInt((date12.toString().substring(5, 7)));

			if (mois1 == moisNow) {

				girls.set(date12.toString(), price1);

			}

		}

		model.addSeries(boys);
		model.addSeries(girls);
		return model;
	}

}
