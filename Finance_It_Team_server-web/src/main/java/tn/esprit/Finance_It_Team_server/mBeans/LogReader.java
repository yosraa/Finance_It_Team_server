package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
public class LogReader {
	  // Singelton

	  private static final LogReader instance = new LogReader();

	  public String logname = "system-log.log";
	  protected String env = System.getProperty("user.dir"); //get the running directory of the user

	  public static Map<String, String> getLogging(File logFile) {
	      Map<String, String> logs = new HashMap<>();
	      FileReader in;
	      try {
	          in = new FileReader(logFile);
	          BufferedReader bufferedReader = new BufferedReader(in);
	          try {
	              String line;
	              while ((line = bufferedReader.readLine()) != null) {
	                  logs.put(line.substring(0, line.indexOf(" ")), line.substring(line.indexOf(" "), line.length()));
	              }
	              in.close();
	          } catch (IOException ex) {
	              Logger.getLogger(LogReader.class.getName()).log(Level.SEVERE, null, ex);
	          }

	      } catch (FileNotFoundException ex) {
	          System.out.println("Cannot open logging file");
	      }
	      return logs;
	  }

	  private LogReader() {   if (instance != null) {
          throw new IllegalStateException("Cannot instantiate a new singleton instance of log");
      }
  }

}

