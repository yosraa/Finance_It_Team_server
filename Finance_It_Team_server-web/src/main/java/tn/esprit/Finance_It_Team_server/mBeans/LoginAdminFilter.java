package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/PagesAdmin/*")
public class LoginAdminFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.print(" url = " + ((HttpServletRequest) request).getRequestURL());
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse =(HttpServletResponse) response;
		
		LoginBean loginBean=(LoginBean) servletRequest.getSession().getAttribute("loginBean");
		if( (loginBean != null && loginBean.isLoggedIn() && loginBean.getUserRole()==2)){
			chain.doFilter(servletRequest, servletResponse);
		}
		else{
			servletResponse.sendRedirect(servletRequest.getContextPath()+ "/PagesLogin/login.jsf");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	
}
