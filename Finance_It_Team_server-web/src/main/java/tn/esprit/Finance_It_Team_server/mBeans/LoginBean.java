package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


import tn.esprit.Finance_It_Team_server.services.ServiceUser;



@ManagedBean
@SessionScoped
public class LoginBean {
	private String login;
	private String password;
	private int userId;
	private int userRole;
	private String email;
	private boolean loggedIn;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserRole() {
		return userRole;
	}

	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}

	public ServiceUser getServiceUser() {
		return serviceUser;
	}

	public void setServiceUser(ServiceUser serviceUser) {
		this.serviceUser = serviceUser;
	}



	@EJB
	ServiceUser serviceUser;

	public String doLogin() {
		String navigateTo = "null";
		List<Integer> idAndRole=serviceUser.login(login, password);
		
		userId=idAndRole.get(0);
		userRole=idAndRole.get(1);

		if(userId==-1){
			return "/PagesLogin/login?faces-redirect=true";
		}
		else{
			
			if(userRole==1)
			{
				navigateTo="/Pages/index?faces-redirect=true";
				loggedIn=true;
			}
			else {
				navigateTo="/PagesAdmin/index?faces-redirect=true";
				loggedIn=true;
			}
			
		}
		

		return navigateTo;
	}
	public void dosendPassword(){
		serviceUser.sendMailWithNewPassword(email);
	}
	public String doforgetPassword(){
		String navigateTo="/PagesLogin/forgotPassword?faces-redirect=true";
		return navigateTo;
	}
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}


	
	public String Logout(){
		loggedIn=false;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/PagesLogin/login?faces-redirect=true";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
