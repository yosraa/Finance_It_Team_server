package tn.esprit.Finance_It_Team_server.mBeans;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Schedules;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import tn.esprit.Finance_It_Team_server.services.ServiceUser;

@Startup
@Singleton
public class LogsCleanUp {
	
	@EJB
	ServiceUser ServiceUser;
	
	
		@Schedules ({
		   @Schedule(dayOfMonth="Last"),
		   @Schedule(dayOfWeek="Fri", hour="23")
		})
	public void doCleanupLogs() { 
		ServiceUser.cleanLogs();
	}
		@Schedules({
			@Schedule(dayOfMonth="3",hour="19" ,minute="38")
		})
		public void doTestingCleanupLogs() { 
			System.out.println("");
			System.out.println("hh0000000g");
		}
		
		@Schedules({
			@Schedule(hour="7" ,minute="4")
		})
		public void doRemainderToTrader() { 
			ServiceUser.sendMailIfUserDidNotLoginFor(3);
		}
}
