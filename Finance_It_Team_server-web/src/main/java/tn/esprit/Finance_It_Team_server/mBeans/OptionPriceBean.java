package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.Finance_It_Team_server.services.Volatility;
import tn.esprit.Finance_It_Team_server.services.VolatilityRemote;

import org.primefaces.event.SelectEvent;

import tn.esprit.Finance_It_Team_server.entities.RealData;
import tn.esprit.Finance_It_Team_server.services.CalculService;
import tn.esprit.Finance_It_Team_server.services.Greeks;
import tn.esprit.Finance_It_Team_server.services.IRealData;
import tn.esprit.Finance_It_Team_server.services.RealDataRemote;

@ManagedBean
@SessionScoped
public class OptionPriceBean implements Serializable {

	private double strikeprice = 0;
	private double currentprice;
	private String TypeOption;
	private double Volatility;
	private double Risk;
	private Date ExpirationDate;
	private String CalculationModel;
	private double DividendYield;
	private double premiumprice;
	private double time;

	@EJB
	CalculService calculService;

	@EJB
	Volatility volatilityservice;

	@EJB
	RealDataRemote realDataservice;

	/********************************/
	private double deltacall;
	private double deltaput;
	private double vegacall;
	private double vegaput;
	private double rhocall;
	private double rhoput;
	private double gammacall;
	private double gammaput;
	@EJB
	Greeks greeksservice;

	public double getDeltacall() {
		return deltacall;
	}

	public void setDeltacall(double deltacall) {
		this.deltacall = deltacall;
	}

	public double getDeltaput() {
		return deltaput;
	}

	public void setDeltaput(double deltaput) {
		this.deltaput = deltaput;
	}

	public double getVegacall() {
		return vegacall;
	}

	public void setVegacall(double vegacall) {
		this.vegacall = vegacall;
	}

	public double getVegaput() {
		return vegaput;
	}

	public void setVegaput(double vegaput) {
		this.vegaput = vegaput;
	}

	public double getRhocall() {
		return rhocall;
	}

	public void setRhocall(double rhocall) {
		this.rhocall = rhocall;
	}

	public double getRhoput() {
		return rhoput;
	}

	public void setRhoput(double rhoput) {
		this.rhoput = rhoput;
	}

	public double getGammacall() {
		return gammacall;
	}

	public void setGammacall(double gammacall) {
		this.gammacall = gammacall;
	}

	public double getGammaput() {
		return gammaput;
	}

	public void setGammaput(double gammaput) {
		this.gammaput = gammaput;
	}

	public Greeks getGreeksservice() {
		return greeksservice;
	}

	public void setGreeksservice(Greeks greeksservice) {
		this.greeksservice = greeksservice;
	}

	public double getPremiumprice() {
		return premiumprice;
	}

	public void setPremiumprice(double premiumprice) {
		this.premiumprice = premiumprice;
	}

	public CalculService getCalculService() {
		return calculService;
	}

	public void setCalculService(CalculService calculService) {
		this.calculService = calculService;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public double getStrikeprice() {
		return strikeprice;
	}

	public OptionPriceBean() {
		super();
	}

	public OptionPriceBean(double strikeprice, double currentprice, String typeOption, double volatility, double risk,
			Date expirationDate, String calculationModel, double dividendYield) {
		super();
		this.strikeprice = strikeprice;
		this.currentprice = currentprice;
		this.TypeOption = typeOption;
		this.Volatility = volatility;
		this.Risk = risk;
		this.ExpirationDate = expirationDate;
		this.CalculationModel = calculationModel;
		this.DividendYield = dividendYield;
	}

	public void setStrikeprice(double strikeprice) {
		this.strikeprice = strikeprice;
	}

	public double getCurrentprice() {
		return currentprice;
	}

	public void setCurrentprice(double currentprice) {
		this.currentprice = currentprice;
	}

	public String getTypeOption() {
		return TypeOption;
	}

	public void setTypeOption(String typeOption) {
		TypeOption = typeOption;
	}

	public double getVolatility() {
		return Volatility;
	}

	public void setVolatility(double volatility) {
		Volatility = volatility;
	}

	public double getRisk() {
		return Risk;
	}

	public void setRisk(double risk) {
		Risk = risk;
	}

	public Date getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		ExpirationDate = expirationDate;
	}

	public String getCalculationModel() {
		return CalculationModel;
	}

	public void setCalculationModel(String calculationModel) {
		CalculationModel = calculationModel;
	}

	public double getDividendYield() {
		return DividendYield;
	}

	public void setDividendYield(double dividendYield) {
		DividendYield = dividendYield;
	}

	public double calculatrice() {
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		long res = calculService.getDateDiff(today, ExpirationDate, TimeUnit.DAYS);
		time = (double) res / 365;
		if (time > 0) {
			if (TypeOption.equalsIgnoreCase("call") && CalculationModel.equals("Black and Scholes")) {
				premiumprice = calculService.callPrice(currentprice, strikeprice, Risk, Volatility, time);

			} else if (TypeOption.equalsIgnoreCase("put") && CalculationModel.equals("Black and Scholes"))
				premiumprice = calculService.putPrice(currentprice, strikeprice, Risk, Volatility, time);
			else if (TypeOption.equalsIgnoreCase("call") && CalculationModel.equals("Binomial")) {
				premiumprice = calculService.callOptionValue(currentprice, strikeprice, time, Risk, Volatility,
						DividendYield);

			} else if (TypeOption.equalsIgnoreCase("put") && CalculationModel.equals("Binomial")) {
				premiumprice = calculService.putOptionValue(currentprice, strikeprice, time, Risk, Volatility,
						DividendYield);

			} else if (TypeOption.equalsIgnoreCase("put") && CalculationModel.equals("Parity Put/Call")) {
				premiumprice = calculService.Parity(time, currentprice, strikeprice, Risk, Volatility, DividendYield,
						"put");

			} else if (TypeOption.equalsIgnoreCase("call") && CalculationModel.equals("Parity Put/Call")) {
				premiumprice = calculService.Parity(time, currentprice, strikeprice, Risk, Volatility, DividendYield,
						"call");

			}

		}
		setgreeksvalues();
		System.out.println(premiumprice);
		return premiumprice;

	}

	public void affichdate() {
		System.out.println("strike price is " + strikeprice + "\n current price is " + currentprice + "\n type "
				+ TypeOption + "\n vol " + Volatility + "\n risk " + Risk + "\n date " + ExpirationDate + "\n model "
				+ CalculationModel + "\n yield " + DividendYield);

		double z = calculatrice();
		System.out.println(z);

	}

	@Override
	public String toString() {
		return "OptionPriceBean [strikeprice=" + strikeprice + ", currentprice=" + currentprice + ", TypeOption="
				+ TypeOption + ", Volatility=" + Volatility + ", Risk=" + Risk + ", ExpirationDate=" + ExpirationDate
				+ ", CalculationModel=" + CalculationModel + ", DividendYield=" + DividendYield + "]";
	}

	public void onDateSelect(SelectEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		facesContext.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
	}

	public void setgreeksvalues() {
		double s = currentprice;
		double k = strikeprice;
		double sigma = Volatility;
		double t = time;
		double r = Risk;
		double q = DividendYield;

		System.out.println(" s= " + s + " k= " + k + " sigma = " + sigma + " t = " + t + " r = " + r + " q= " + q);

		deltacall = greeksservice.delta(s, k, r, sigma, t, q, "call");
		deltaput = greeksservice.delta(s, k, r, sigma, t, q, "put");
		vegacall = greeksservice.vega(s, k, r, sigma, t, q);
		vegaput = greeksservice.vega(s, k, r, sigma, t, q);
		rhocall = greeksservice.rho(s, k, r, sigma, t, "call");
		rhoput = greeksservice.rho(s, k, r, sigma, t, "put");
		gammaput = greeksservice.gamma(s, k, r, sigma, t, q);
		gammacall = gammaput;
	}

	public double calculatevolatility() {

		ArrayList<Float> allClose = new ArrayList<>();

		List<RealData> o = realDataservice.getAllrealdata();
		for (RealData e : o) {
			allClose.add((float) e.getClose());
		}

		double d;
		d = volatilityservice.volatilty_value(allClose);
		Volatility=d;
		return d;
	}

}