package tn.esprit.Finance_It_Team_server.mBeans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

import javafx.scene.chart.XYChart;
import tn.esprit.Finance_It_Team_server.services.CalculService;

@ManagedBean
@SessionScoped
public class PayoffChartBean implements Serializable {

	private LineChartModel lineModel1;
	private LineChartModel lineModel2;

	@EJB
	CalculService calculservice;

	public LineChartModel init(double strike, double premium, String OptionType) {
		System.out.println("strike price is : " + strike + " premium price is " + premium);
		return createLineModels(strike, premium, OptionType);
	}

	public LineChartModel getLineModel1() {
		return lineModel1;
	}

	private LineChartModel createLineModels(double strike, double premium, String OptionType) {
		lineModel1 = initLinearModel(strike, premium, OptionType);
		lineModel1.setTitle("Linear Chart");
		lineModel1.setLegendPosition("e");
		lineModel1.setZoom(true);
		Axis yAxis = lineModel1.getAxis(AxisType.Y);
		Axis xAxis = lineModel1.getAxis(AxisType.X);
		xAxis.setMax(strike * 51);
		xAxis.setMin(-strike * 51);
		yAxis.setMin(-premium * 10);
		yAxis.setMax(premium * 10);
		yAxis.setLabel("Gain");
		xAxis.setLabel("Current Price");
		return lineModel1;

	}

	private LineChartModel initLinearModel(double strike, double premium, String OptionType) {
		LineChartModel model = new LineChartModel();

		LineChartSeries series1 = new LineChartSeries();
		LineChartSeries series2 = new LineChartSeries();

		if (OptionType.equalsIgnoreCase("call")) {
			series1.setLabel("Long Call");
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) ((strike))) {

				double gain = calculservice.maxstk(i, strike) - premium;
				series1.set(i, gain);

			}
			series2.setLabel("Short Call");
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) (strike)) {

				double gain = calculservice.maxstk(i, strike) - premium;
				series2.set(i, -gain);
			}
		} else if (OptionType.equalsIgnoreCase("put")) {
			series1.setLabel("Long put");
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) (strike)) {

				double gain = calculservice.maxkst(strike, i) - premium;
				series1.set(i, gain);

			}
			series2.setLabel("Short put");
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) (strike)) {

				double gain = calculservice.maxkst(strike, i) - premium;
				series2.set(i, -gain);

			}

		}
		model.addSeries(series1);
		model.addSeries(series2);

		return model;
	}

}