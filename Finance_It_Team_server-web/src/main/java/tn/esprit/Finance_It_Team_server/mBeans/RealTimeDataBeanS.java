package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.json.JSONArray;
import org.json.JSONObject;
import com.github.kevinsawicki.http.HttpRequest;

import tn.esprit.Finance_It_Team_server.entities.Stocks;
import tn.esprit.Finance_It_Team_server.services.StockContractService;
import tn.esprit.Finance_It_Team_server.mBeans.SystemLog;


@ManagedBean
@ViewScoped
public class RealTimeDataBeanS implements Serializable {

	private static final long serialVersionUID = 1L;
	private String symbole;
	private String symbols;
	private   static final String ACTIONCHECK = "CHECK:";
	private   static final String ACTIONCALCUL = "CALCULATE:";
	private   static final String ACTIONDATA = "REAL_TIME_DATA:";
	private   static final String ACTIONEMAIL = "CONTACT:";
	@EJB
	StockContractService stockContractservice;

	public static final String CLOSE = "close";
	private static final String HIGH = "high";
	private static final String LOW = "low";
	private static final String NAME = "name";
	private static final String OPEN = "open";
	private static final String SYMBOL = "symbol";
	private static final String VOLUME = "volume";

	private List<Stocks> listStock;

	@PostConstruct
	public void init() {
		listStock = showStockData();
	}



	public StockContractService getStockContractservice() {
		return stockContractservice;
	}



	public void setStockContractservice(StockContractService stockContractservice) {
		this.stockContractservice = stockContractservice;
	}



	public static String getClose() {
		return CLOSE;
	}

	public static String getHigh() {
		return HIGH;
	}

	public static String getLow() {
		return LOW;
	}

	public static String getName() {
		return NAME;
	}

	public static String getOpen() {
		return OPEN;
	}

	public static String getSymbol() {
		return SYMBOL;
	}

	public static String getVolume() {
		return VOLUME;
	}

	public List<Stocks> showStockData() {
		
		/*
		 * String response =
		 * HttpRequest.get("https://marketdata.websol.barchart.com" +
		 * "/getQuote.json?apikey=5762666332e2d8a7b5374845ed4c5513&symbols=AMZN"
		 * + "ZC*1,IBM,GOOGL,%5EEURUSD,AMZN,AAPL,FB,NFLX,INTC," +
		 * "TGT,BAC,PIH,TSLA,BABA,F,MU,INTC,GE,TWTR,WMT,NOK," +
		 * "QQQ,WDC,ECYT,ADMP,GM,TI,SRPT,LRCX,TRXC,TSRO,CSPI," +
		 * "CASA,BB,GILD,XOM,PBR,AXP,QCOM,FIT,UAA,DAL,CVS")
		 * .accept("application/json").body();
		 * 
		 * JSONObject jsonObject = new JSONObject(response);
		 * 
		 * JSONArray result = jsonObject.getJSONArray("results"); for (int i =
		 * 0; i < result.length(); i++) { Stocks stocks = new Stocks();
		 * stocks.setRef(i + "");
		 * stocks.setClose(result.getJSONObject(i).getString(CLOSE));
		 * stocks.setHigh(result.getJSONObject(i).getString(HIGH));
		 * stocks.setLow(result.getJSONObject(i).getString(LOW));
		 * stocks.setName(result.getJSONObject(i).getString(NAME));
		 * stocks.setOpen(result.getJSONObject(i).getString(OPEN));
		 * stocks.setSymbol(result.getJSONObject(i).getString(SYMBOL));
		 * stocks.setVolume(result.getJSONObject(i).getString(VOLUME));
		 * listStock.add(stocks); } return listStock;
		 */

		String response = HttpRequest.get("https://marketdata.websol.barchart.com"
				+ "/getQuote.json?apikey=5762666332e2d8a7b5374845ed4c5513&symbols="
				+ "ZC*1,IBM,GOOGL,%5EEURUSD,AMZN,AAPL,FB,NFLX,INTC,"
				+ "TGT,BAC,PIH,TSLA,BABA,F,MU,INTC,GE,TWTR,WMT,NOK,"
				+ "QQQ,WDC,ECYT,ADMP,GM,TI,SRPT,LRCX,TRXC,TSRO,CSPI," + "CASA,BB,GILD,XOM,PBR,AXP,QCOM,FIT,UAA,DAL,CVS")
				.accept("application/json").body();

		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("status");
		String strCode = status.getDouble("code") + "";
		if (!strCode.equals("200.0"))
			Collections.emptyList();
		//
		JSONArray result = jsonObject.getJSONArray("results");
		List<Stocks> lst = new ArrayList<>();
		for (int i = 0; i < result.length(); i++) {
			JSONObject data = result.getJSONObject(i);
			Stocks stocks = new Stocks();
			stocks.setRef(i + "");
			stocks.setClose(data.getDouble("close") + "");
			stocks.setHigh(data.getDouble("high") + "");
			stocks.setLow(data.getDouble("low") + "");
			stocks.setName(data.getString("name"));
			stocks.setOpen(data.getDouble("open") + "");
			stocks.setSymbol(data.getString("symbol"));

			stocks.setVolume(data.getInt("volume") + "");
			stocks.setName(data.getString("name"));
			stocks.setGainToBuy(Double.parseDouble(stocks.getHigh()) * Double.parseDouble(stocks.getOpen()));
			stocks.setGainToSell(Double.parseDouble(stocks.getOpen()) * Double.parseDouble(stocks.getLow()));
			stocks.setOhlc((Double.parseDouble(stocks.getClose()) * Double.parseDouble(stocks.getHigh())
					* Double.parseDouble(stocks.getLow()) * Double.parseDouble(stocks.getOpen())) / 4);
			stocks.setDsa((Double.parseDouble(stocks.getHigh()) - Double.parseDouble(stocks.getOpen()))
					* Double.parseDouble(stocks.getOpen())
					- Double.parseDouble(stocks.getLow())
							/ ((Double.parseDouble(stocks.getHigh()) - Double.parseDouble(stocks.getLow()))
									* (Double.parseDouble(stocks.getHigh()) - Double.parseDouble(stocks.getLow()))));

			lst.add(stocks);
		}
		SystemLog.log(ACTIONDATA + "STOCKS");

		return lst;

	}

	public List<Stocks> getListStock() {
		return listStock;
	}

	public void setListStock(List<Stocks> listStock) {
		this.listStock = listStock;
	}

	public List<Stocks> showHistoriqueDataBySymbole() {
		String[] symbols = { "AMZN", "AAPL", "BAC", "BMW", "DIS", "INTC", "GOOG", "NFLX", "MSFT", "WMT" };
		String s = "GOOG";
		/*
		 * String response = HttpRequest .get(
		 * "https://marketdata.websol.barchart.com/getHistory.json?apikey=5762666332e2d8a7b5374845ed4c5513&symbol="
		 * + symbole + "&type=daily&startDate=20180100000000")
		 * .accept("application/json").body();
		 */
		// System.out.println("*****************avant" + response);
		// for (int i = 0; i < symbols.length; i++) {

		String response = HttpRequest
				.get("https://marketdata.websol.barchart.com/getHistory.json?apikey=5762666332e2d8a7b5374845ed4c5513&symbol="
						+ s + "&type=daily&startDate=20180100000000")
				.accept("application/json").body();
		System.out.println("*****************apreeees1" + response);

		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("status");
		String strCode = status.getDouble("code") + "";
		if (!strCode.equals("200.0"))
			return Collections.emptyList();
		//
		JSONArray result = jsonObject.getJSONArray("results");
		List<Stocks> listStock = new ArrayList<>();
		for (int j = 0; j < result.length(); j++) {
			JSONObject data = result.getJSONObject(j);
			Stocks stocks = new Stocks();
			stocks.setRef(j + "");
			stocks.setSymbol(data.getString("symbol") + "");
			stocks.setClose(data.getDouble("close") + "");
			stocks.setHigh(data.getDouble("high") + "");
			stocks.setLow(data.getDouble("low") + "");
			stocks.setOpen(data.getDouble("open") + "");
			stocks.setVolume(data.getInt("volume") + "");
			listStock.add(stocks);
		}
		// }
		SystemLog.log(ACTIONDATA + "Historic data GOOGLE");

		return listStock;
	}

	public String getSymbole() {
		return symbole;
	}

	public void setSymbole(String symbole) {
		this.symbole = symbole;
	}

	public String show() {

		showHistoriqueDataBySymbole();

		return "RealTimeDataS?faces-redirect=true";

	}

	public String getSymbols() {
		return symbols;
	}

	public void setSymbols(String symbols) {
		this.symbols = symbols;
	}



}
