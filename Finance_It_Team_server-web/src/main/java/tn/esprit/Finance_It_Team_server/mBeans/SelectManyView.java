package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
public class SelectManyView {

	private List<String> unities;
	private List<String> selectedUnities;
	private List<String> selectedOptions;

	@ManagedProperty("#{unityService}")
	private UnityService service;

	@PostConstruct
	public void init() {
		unities = service.getUnities();
	}

	public List<String> getThemes() {
		return unities;
	}

	public void setService(UnityService service) {
		this.service = service;
	}

	public List<String> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(List<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public List<String> getSelectedThemes() {
		return selectedUnities;
	}

	public void setSelectedThemes(List<String> selectedThemes) {
		this.selectedUnities = selectedThemes;
	}

}