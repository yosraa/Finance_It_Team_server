package tn.esprit.Finance_It_Team_server.mBeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class SessionOptionBean {
	private int idStockOption;

	public int getIdStockOption() {
		return idStockOption;
			
	}

	public void setIdStockOption(int idStockOption) {

		this.idStockOption = idStockOption;
	}
}
