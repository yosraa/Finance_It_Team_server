package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.services.ServiceUser;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;

@ManagedBean
@ViewScoped
public class SignUp {
	private String prenom;
	private String username;
	private String nom;
	private String password;
	private String email;
	
	@EJB
	ServiceUser serviceUser;
	
	public SignUp() {
	}	
	public void doSignUp() throws NamingException {
		Trader trader=new Trader(username,nom,prenom,email,password,"M",0,"0","pending");
		serviceUser.addUser(trader);

	}
	
	  public String traderControl(Trader trader) throws NamingException{
	    	if(trader.getUserName().isEmpty())return"username empty";
	    	if(trader.getFirstName().isEmpty())return"first name empty";
	    	if(trader.getLastName().isEmpty())return"last name empty";
	    	if(trader.getPassword().isEmpty())return"password empty";
	    	if(trader.getEmail().isEmpty())return"email empty";
	    	if(checkString(trader.getUserName()).equals("no"))return"no special characters in username";
	    	if(checkString(trader.getFirstName()).equals("no"))return"no special characters in First name";
	    	if(checkString(trader.getLastName()).equals("no"))return"no special characters in Last name";
	    	if(checkString(trader.getPassword()).equals("no"))return"no special characters in password";
	    	if(checkEmail(trader.getEmail()).equals("no"))return"email is not valid";
	    	
	    	
	    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
			Context context=new InitialContext();
			ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
			if(proxy.emailExists(trader.getEmail()))return"email already exists";
			if(proxy.usernameExists(trader.getUserName()))return"username already exists";

			return "ok";
	    	
	    	
	    }
	  public String checkString(String string){

		  Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		  Matcher m = p.matcher(string);
		  boolean b = m.find();
		  if (b)return("no");
		  return("ok");
		      }
		      public String checkEmail(String email){

		      	Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		      	Matcher m = p.matcher(email);
		      	boolean matchFound = m.matches();
		      	if (matchFound) {
		      		return "ok" ;   	
		      	    }
		      	    return "no";
		      }
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ServiceUser getServiceUser() {
		return serviceUser;
	}
	public void setServiceUser(ServiceUser serviceUser) {
		this.serviceUser = serviceUser;
	}
	
	

}
