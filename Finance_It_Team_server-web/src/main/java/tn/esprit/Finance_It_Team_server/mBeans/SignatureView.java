package tn.esprit.Finance_It_Team_server.mBeans;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class SignatureView {
 
    private String value;
 
    public String getValue() {
        return value;
    }
 
    public void setValue(String value) {
        this.value = value;
    }
}