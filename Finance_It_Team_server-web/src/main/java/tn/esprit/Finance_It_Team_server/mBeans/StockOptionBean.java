package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;
import tn.esprit.Finance_It_Team_server.mBeans.LoginBean;
import tn.esprit.Finance_It_Team_server.mBeans.SystemLog;



@ViewScoped
//@SessionScoped
// @ApplicationScoped
@ManagedBean
public class StockOptionBean {
	private float strikePriceStock;
	private float currentPriceStock;
	private TypeOptionStock typeOptionStock; // enum hedhi lezemha classe ennum
	private Date expirationDateStock; // nzid nchouf le type mta3 l date
	private float volatilityStock;
	private float riskStock;
	private float timeStock;
	private StateOptionStock stateOptionStock; // enum hedhi lezemha classe
												// ennum
	private Date dateOptionBegin;
	private Date dateOptionEnd;
	private float nbShares;
	private List<StockOption> stockOptions;
	private Integer stockOptionIdToBeUpdated;
	public static int idStock;
	private String value;
	private List<StockOption> myStockOption;
	private   static final String ACTIONADD = "ADD:";
	private   static final String ACTIONUPDATE = "UPDATE:";
	private   static final String ACTIONCHECK = "CHECK:";
	private   static final String ACTIONCALCUL = "CALCULATE:";
	private   static final String ACTIONDELETE = "DELETE:";
	private   static final String ACTIONDATA = "REAL_TIME_DATA:";
	private   static final String ACTIONEMAIL = "CONTACT:";
	private   static final String ACTIONBUY = "Buy & Sell:";





	


	@EJB
	StockOptionServiceRemote stockOptionService;

	@ManagedProperty(value = "#{loginBean}")
	LoginBean loginBean;

	@ManagedProperty(value = "#{sessionOptionBean}")
	SessionOptionBean sessionOptionBean;

	@PostConstruct // elle s'execute juste apres (l instantiation du bean et l
					// injection de dependance(le responsable aliha @EJB w le
					// composant logiciel qui le fait seveur d application "ejb
					// container"))
	public void init() {
		System.out.println("***********"+loginBean.getUserId());
	
		expirationDateStock = new Date();
		dateOptionBegin = new Date();
		dateOptionEnd = new Date();
		stockOptions = stockOptionService.findOptionsByTrader(loginBean.getUserId());

	}

	String navigateTo = "null";

	public SessionOptionBean getSessionOptionBean() {
		return sessionOptionBean;
	}

	public void setSessionOptionBean(SessionOptionBean sessionOptionBean) {
		this.sessionOptionBean = sessionOptionBean;
	}


	public String addStockOption() {
		 if (loginBean == null || !loginBean.isLoggedIn()) {
		 return "/Pageslogin/login.jsf";
		 }
		StockOption stockOption = new StockOption(strikePriceStock, currentPriceStock, typeOptionStock,
				expirationDateStock, volatilityStock, riskStock, timeStock, stateOptionStock.no, dateOptionBegin,
				dateOptionEnd, nbShares);
		stockOptionService.addStockOption(stockOption, loginBean.getUserId());
		stockOptions.add(stockOption);
		
		SystemLog.log(ACTIONADD + "a Stock Option");
		return "ListStockOption?faces-redirect=true";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static int getIdStock() {
		return idStock;
	}

	public static void setIdStock(int idStock) {
		StockOptionBean.idStock = idStock;
	}

	public List<StockOption> getMyStockOption() {
		myStockOption = stockOptionService.findOptionsByTrader(loginBean.getUserId());
		return myStockOption;
	}

	public void setMyStockOption(List<StockOption> myStockOption) {
		this.myStockOption = myStockOption;
	}

	public String modifier(StockOption stockOption) {
		 if (loginBean == null || !loginBean.isLoggedIn()) {
			 return "/Pageslogin/login.jsf";
			 }
		sessionOptionBean.setIdStockOption(stockOption.getId());

		this.setStrikePriceStock(stockOption.getStrikePriceStock());
		this.setCurrentPriceStock(stockOption.getCurrentPriceStock());
		this.setTypeOptionStock(stockOption.getTypeOptionStock());

		this.setExpirationDateStock(stockOption.getExpirationDateStock());
		this.setVolatilityStock(stockOption.getVolatilityStock());
		this.setRiskStock(stockOption.getRiskStock());
		this.setTimeStock(stockOption.getTimeStock());

		this.setStateOptionStock(stockOption.getStateOptionStock());
		this.setDateOptionBegin(stockOption.getDateOptionBegin());
		this.setDateOptionEnd(stockOption.getDateOptionEnd());
		this.setNbShares(stockOption.getNbShares());
		SystemLog.log(ACTIONUPDATE+ "a Stock Option");

		return "UpdateStockOption?faces-redirect=true";

	}

	public String supprimer(int optionId) {
		 if (loginBean == null || !loginBean.isLoggedIn()) {
			 return "/Pageslogin/login.jsf";
			 }
		stockOptionService.findStockOptioneById(optionId);
		stockOptionService.deleteStockOption(optionId);
		SystemLog.log(ACTIONDELETE + "a Stock Option");

		return "ListStockOption?faces-redirect=true";

	}



	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public String GainLossBtn(int id) {
		String DeepInOutAtTheMoney;
		DeepInOutAtTheMoney = stockOptionService.DeepInOutAtTheMoney(id);
	//	SystemLog.log(ACTIONCALCUL + "if it's a Gain or Loss Option");

		return DeepInOutAtTheMoney;
	}

	public float intrinsecValue(int id) {
		float intrinsecValue;
		intrinsecValue = stockOptionService.StockOptionPutCall(id);
	//	SystemLog.log(ACTIONCALCUL + "The Intrinsec Value of a Stock Option ");

		return intrinsecValue;
	}

	public float timeValue(int id) {
		float timeValue;
		timeValue = stockOptionService.StockOptionTimeValue(id);
	//	SystemLog.log(ACTIONCALCUL + "The Time Value of a Stock Option ");

		return timeValue;
	}

	public float futurePrice(int id) {
		float fprice;
		fprice = stockOptionService.futuresPrice(id);
	//	SystemLog.log(ACTIONCALCUL + "FUTURE Price of a Stock Option");

		return fprice;
	}

	public float premium(int id) {
		float p;
		p = stockOptionService.OptionPriceFormula(id);
	//	SystemLog.log(ACTIONCALCUL + "Premium of a Stock Option");

		return p;
	}

	/*
	 * public float loockBack(float S, float Smin, float r, float q, float
	 * sigma, float time){ float l;
	 * l=stockOptionService.PriceOfALookbackOption(S, Smin,r, q, sigma, time);
	 * return l; }
	 */

	public float warrantPricing(int id) {
		float w;
		w = stockOptionService.WarrantPriceAdjustedBS(id);
		SystemLog.log(ACTIONCALCUL + "a Stock Option using WARRANT method");

		return w;
	}

	public float nbSharesValue(int id) {
		float n = 5;
		// n=stockOptionService.calculValueShares(id);
	//	SystemLog.log(ACTIONCHECK + "Shares Value of the Stock Option");


		return n;
	}

	public float getStrikePriceStock() {
		return strikePriceStock;
	}

	public void setStrikePriceStock(float strikePriceStock) {
		this.strikePriceStock = strikePriceStock;
	}

	public float getCurrentPriceStock() {
		return currentPriceStock;
	}

	public void setCurrentPriceStock(float currentPriceStock) {
		this.currentPriceStock = currentPriceStock;
	}

	public TypeOptionStock getTypeOptionStock() {
		return typeOptionStock;
	}

	public void setTypeOptionStock(TypeOptionStock typeOptionStock) {
		this.typeOptionStock = typeOptionStock;
	}

	public Date getExpirationDateStock() {
		return expirationDateStock;
	}

	public void setExpirationDateStock(Date expirationDateStock) {
		this.expirationDateStock = expirationDateStock;
	}

	public float getVolatilityStock() {
		return volatilityStock;
	}

	public void setVolatilityStock(float volatilityStock) {
		this.volatilityStock = volatilityStock;
	}

	public float getRiskStock() {
		return riskStock;
	}

	public void setRiskStock(float riskStock) {
		this.riskStock = riskStock;
	}

	public float getTimeStock() {
		return timeStock;
	}

	public void setTimeStock(float timeStock) {
		this.timeStock = timeStock;
	}

	public StateOptionStock getStateOptionStock() {
		return stateOptionStock;
	}

	public void setStateOptionStock(StateOptionStock stateOptionStock) {
		this.stateOptionStock = stateOptionStock;
	}

	public Date getDateOptionBegin() {
		return dateOptionBegin;
	}

	public void setDateOptionBegin(Date dateOptionBegin) {
		this.dateOptionBegin = dateOptionBegin;
	}

	public Date getDateOptionEnd() {
		return dateOptionEnd;
	}

	public void setDateOptionEnd(Date dateOptionEnd) {
		this.dateOptionEnd = dateOptionEnd;
	}

	public float getNbShares() {
		return nbShares;
	}

	public void setNbShares(float nbShares) {
		this.nbShares = nbShares;
	}

	public List<StockOption> getStockOptions() {
		return stockOptions;
	}

	public void setStockOptions(List<StockOption> stockOptions) {
		this.stockOptions = stockOptions;
	}

	public StockOptionServiceRemote getStockOptionService() {
		return stockOptionService;
	}

	public void setStockOptionService(StockOptionServiceRemote stockOptionService) {
		this.stockOptionService = stockOptionService;
	}

	public Integer getStockOptionIdToBeUpdated() {
		return stockOptionIdToBeUpdated;
	}

	public void setStockOptionIdToBeUpdated(Integer stockOptionIdToBeUpdated) {
		this.stockOptionIdToBeUpdated = stockOptionIdToBeUpdated;
	}

	public void save(ActionEvent actionEvent) {
		addMessage("Data saved");
	}

	public void update(ActionEvent actionEvent) {
		addMessage("Data updated");
	}

	public void delete(ActionEvent actionEvent) {
		addMessage("Data deleted");
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

}
