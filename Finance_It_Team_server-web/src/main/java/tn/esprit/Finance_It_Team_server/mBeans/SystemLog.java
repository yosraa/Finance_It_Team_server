package tn.esprit.Finance_It_Team_server.mBeans;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//Momo Ouss Log System
public class SystemLog {
	 // Singelton

    private static final SystemLog instance = new SystemLog();

    public String logname = "system-log.log";
    protected String env = System.getProperty("user.dir"); //get the running directory of the user
    public static File logFile;

    public static SystemLog getInstance() {
        return instance;
    }

    public static SystemLog getInstance(String withName) {
        instance.logname = withName;
        instance.createLogFile();
        return instance;
    }

    public void createLogFile() {
        // Determine if a logs directory exists or not.
        File logsFolder = new File(env + '/' + "logs");
        if (!logsFolder.exists()) {
            // Create the directory
            System.err.println("INFO: Creating new logs directory in " + env);
            logsFolder.mkdir();
        }

        
        SystemLog.logFile = new File(logsFolder.getName(), logname);
        if (!SystemLog.logFile.exists()) {
            try {
                if (logFile.createNewFile()) {
                    // We need to create a new file
                    System.err.println("INFO: Creating new log file");
                }
            } catch (IOException e) {
                // This can be thrown due to writing system rights
                System.err.println("ERROR: Cannot create log file");
            }

        }
    }

    private SystemLog() {
        if (instance != null) {
            throw new IllegalStateException("Cannot instantiate a new singleton instance of log");
        }
        this.createLogFile();
    }

    public static void log(String message) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy|hh:mm:ssaa");
            Calendar cal = Calendar.getInstance();
            String actionTime = dateFormat.format(cal.getTime());
            FileWriter out = new FileWriter(SystemLog.logFile, true);
            String toLog = actionTime + " " + message;
            out.write(toLog.toCharArray());
            out.write("\n");
            out.close();
        } catch (IOException e) {
            System.err.println("ERROR: Could not write to log file");
        }
    }
}
