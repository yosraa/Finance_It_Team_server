package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOptionPk;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionService;

@ManagedBean
@SessionScoped
public class TradingCurrencyOptionBean {

	private AchatCurrencyOptionPk achatCurrencyOptionPK;
	private Date settelmentDate;
	private StateTrade stateTrade;
	private CurrencyOption currencyOption;
	private Trader trader;
	private int idTrader;
	private int idCurrencyOption;
	private String firstName;
	private String email;
	private float strikepp;

	private List<AchatCurrencyOption> achatcurrop;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AchatCurrencyOptionService getAchatCurrencyOptionService() {
		return achatCurrencyOptionService;
	}

	public void setAchatCurrencyOptionService(AchatCurrencyOptionService achatCurrencyOptionService) {
		this.achatCurrencyOptionService = achatCurrencyOptionService;
	}

	private List<AchatCurrencyOption> forexTrade;

	@EJB
	AchatCurrencyOptionService achatCurrencyOptionService;

	public List<AchatCurrencyOption> getForexTrade() {
		forexTrade = achatCurrencyOptionService.getAllTradePending();
		return forexTrade;
	}

	public void setForexTrade(List<AchatCurrencyOption> forexTrade) {
		this.forexTrade = forexTrade;
	}

	public AchatCurrencyOptionPk getAchatCurrencyOptionPK() {
		return achatCurrencyOptionPK;
	}

	public void setAchatCurrencyOptionPK(AchatCurrencyOptionPk achatCurrencyOptionPK) {
		this.achatCurrencyOptionPK = achatCurrencyOptionPK;
	}

	public Date getSettelmentDate() {
		return settelmentDate;
	}

	public void setSettelmentDate(Date settelmentDate) {
		this.settelmentDate = settelmentDate;
	}

	public StateTrade getStateTrade() {
		return stateTrade;
	}

	public void setStateTrade(StateTrade stateTrade) {
		this.stateTrade = stateTrade;
	}

	public CurrencyOption getCurrencyOption() {
		return currencyOption;
	}

	public void setCurrencyOption(CurrencyOption currencyOption) {
		this.currencyOption = currencyOption;
	}

	public Trader getTrader() {
		return trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	public int getIdTrader() {
		return idTrader;
	}

	public void setIdTrader(int idTrader) {
		this.idTrader = idTrader;
	}

	public int getIdCurrencyOption() {
		return idCurrencyOption;
	}

	public void setIdCurrencyOption(int idCurrencyOption) {
		this.idCurrencyOption = idCurrencyOption;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	private String nom;

	public void Recuperer(AchatCurrencyOption achatCurrency) {

		this.setIdCurrencyOption(achatCurrency.getCurrencyOption().getId());
		this.setFirstName(achatCurrency.getTrader().getFirstName());
		this.setEmail(achatCurrency.getTrader().getEmail());

	}

	public float strike() {
		float strikepp = achatCurrencyOptionService.getStrikePrice(idCurrencyOption);
		System.out.println(strikepp);
		return strikepp;
	}

	public float amount() {
		float amount = achatCurrencyOptionService.getAmountFromCurrencyAccount();
		System.out.println(amount);
		return amount;
	}

	public float amount10() {
		float amount10 = achatCurrencyOptionService.getAmountFromCurrencyAccount12();
		System.out.println(amount10);
		return amount10;
	}

	public Trader finfTrader11(int id) {
		return achatCurrencyOptionService.findtrader(27);
	}

	public Trader finfTrader12(int id) {
		return achatCurrencyOptionService.findtrader(id);
	}

	public float amount1() {
		return amount() - 123;
	}

	public float amount110() {
		return amount() + 123;
	}

	public void achatForexOp() {
		if (123 < amount())

			achatCurrencyOptionService.ajouterAchatCurrencyOption(idTrader, idCurrencyOption, nom, settelmentDate,
					StateTrade.Accepted);

		achatCurrencyOptionService.setAmount(finfTrader11(idTrader), amount1());
		// achatCurrencyOptionService.setAmount(finfTrader11(24),amount110());
		// amount1();
		System.out.println(amount1());

	}

	public long calculPending() {
		return achatCurrencyOptionService.calculerPending();
	}

	public List<AchatCurrencyOption> getAchatcurrop() {
		achatcurrop = achatCurrencyOptionService.getAllTrade();
		return achatcurrop;
	}

	public void setAchatcurrop(List<AchatCurrencyOption> achatcurrop) {
		this.achatcurrop = achatcurrop;
	}

	@Override
	public String toString() {
		return "TradingCurrencyOptionBean [achatCurrencyOptionPK=" + achatCurrencyOptionPK + ", settelmentDate="
				+ settelmentDate + ", stateTrade=" + stateTrade + ", currencyOption=" + currencyOption + ", trader="
				+ trader + ", idTrader=" + idTrader + ", idCurrencyOption=" + idCurrencyOption + ", firstName="
				+ firstName + ", email=" + email + ", strikepp=" + strikepp + ", achatcurrop=" + achatcurrop
				+ ", forexTrade=" + forexTrade + ", achatCurrencyOptionService=" + achatCurrencyOptionService + ", nom="
				+ nom + "]";
	}

}





