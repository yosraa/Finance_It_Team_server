package tn.esprit.Finance_It_Team_server.mBeans;


import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
 
 
@ManagedBean(name="unityService", eager = true)
@ApplicationScoped
public class UnityService {
     
    private List<String> unities;
     
    @PostConstruct
    public void init() {
        unities = new ArrayList<String>();
        unities.add("EUR");
        unities.add("TND");
        unities.add("USD");
        unities.add("AUD");
        unities.add("CAD");
        unities.add("cny");
        unities.add("inr");
        unities.add("gbp");
        unities.add("jpy");
        unities.add("rub");


        
    }
     
    public List<String> getUnities() {
        return unities;
    } 
}