package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;
import tn.esprit.Finance_It_Team_server.mBeans.LoginBean;
import tn.esprit.Finance_It_Team_server.mBeans.SystemLog;

@ViewScoped
//@SessionScoped
@ManagedBean
public class UpdateStockOptionBean {
	private float strikePriceStock;
	private float currentPriceStock;
	private TypeOptionStock typeOptionStock; // enum hedhi lezemha classe ennum
	private Date expirationDateStock; // nzid nchouf le type mta3 l date
	private float volatilityStock;
	private float riskStock;
	private float timeStock;
	private StateOptionStock stateOptionStock; // enum hedhi lezemha classe
												// ennum
	private Date dateOptionBegin;
	private Date dateOptionEnd;
	private float nbShares;
	private List<StockOption> stockOptions;
	private int stockOptionIdToBeUpdated;
	private Stock stock;
	private   static final String ACTIONCALCULATE = "CALCULATE:";

	@EJB
	StockOptionServiceRemote stockOptionService;

	@ManagedProperty(value = "#{sessionOptionBean}")
	SessionOptionBean sessionOptionBean;

	@ManagedProperty(value = "#{stockOptionBean}")
	StockOptionBean stockOptionBean;

	@ManagedProperty(value = "#{loginBean}")
	LoginBean loginBean;

	@PostConstruct
	public void init() {
		StockOption stockOption = stockOptionService.findStockOptioneById(sessionOptionBean.getIdStockOption());
		this.setStrikePriceStock(stockOption.getStrikePriceStock());
		this.setCurrentPriceStock(stockOption.getCurrentPriceStock());
		this.setTypeOptionStock(stockOption.getTypeOptionStock());

		this.setExpirationDateStock(stockOption.getExpirationDateStock());
		this.setVolatilityStock(stockOption.getVolatilityStock());
		this.setRiskStock(stockOption.getRiskStock());
		this.setTimeStock(stockOption.getTimeStock());

		this.setStateOptionStock(stockOption.getStateOptionStock());
		this.setDateOptionBegin(stockOption.getDateOptionBegin());
		this.setDateOptionEnd(stockOption.getDateOptionEnd());
		this.setNbShares(stockOption.getNbShares());
	}

	public SessionOptionBean getSessionOptionBean() {
		return sessionOptionBean;
	}

	public void setSessionOptionBean(SessionOptionBean sessionOptionBean) {
		this.sessionOptionBean = sessionOptionBean;
	}

	public String mettreAjourStockOption() {

		// stockOptionService.updateStockOption(new
		// StockOption(sessionOptionBean.getIdStockOption(), strikePriceStock,
		// currentPriceStock, typeOptionStock, expirationDateStock,
		// volatilityStock, riskStock, timeStock, stateOptionStock,
		// dateOptionBegin, dateOptionEnd, nbShares));
		StockOption stockOption = (new StockOption(sessionOptionBean.getIdStockOption(), strikePriceStock,
				currentPriceStock, typeOptionStock, expirationDateStock, volatilityStock, riskStock, timeStock,
				stateOptionStock, dateOptionBegin, dateOptionEnd, nbShares));

		stockOptionService.updateStockOptionEtTrader(stockOption, loginBean.getUserId());
		stockOptionBean.setStockOptions(stockOptionService.findOptionsByTrader(loginBean.getUserId()));
		
		SystemLog.log(ACTIONCALCULATE + "Price,Gain,Loss,TimeValue,Intrinsec...");

		return "ListStockOption?faces-redirect=true";
		

	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public float getStrikePriceStock() {
		return strikePriceStock;
	}

	public void setStrikePriceStock(float strikePriceStock) {
		this.strikePriceStock = strikePriceStock;
	}

	public float getCurrentPriceStock() {
		return currentPriceStock;
	}

	public void setCurrentPriceStock(float currentPriceStock) {
		this.currentPriceStock = currentPriceStock;
	}

	public TypeOptionStock getTypeOptionStock() {
		return typeOptionStock;
	}

	public void setTypeOptionStock(TypeOptionStock typeOptionStock) {
		this.typeOptionStock = typeOptionStock;
	}

	public Date getExpirationDateStock() {
		return expirationDateStock;
	}

	public void setExpirationDateStock(Date expirationDateStock) {
		this.expirationDateStock = expirationDateStock;
	}

	public float getVolatilityStock() {
		return volatilityStock;
	}

	public void setVolatilityStock(float volatilityStock) {
		this.volatilityStock = volatilityStock;
	}

	public float getRiskStock() {
		return riskStock;
	}

	public void setRiskStock(float riskStock) {
		this.riskStock = riskStock;
	}

	public float getTimeStock() {
		return timeStock;
	}

	public void setTimeStock(float timeStock) {
		this.timeStock = timeStock;
	}

	public StateOptionStock getStateOptionStock() {
		return stateOptionStock;
	}

	public void setStateOptionStock(StateOptionStock stateOptionStock) {
		this.stateOptionStock = stateOptionStock;
	}

	public Date getDateOptionBegin() {
		return dateOptionBegin;
	}

	public void setDateOptionBegin(Date dateOptionBegin) {
		this.dateOptionBegin = dateOptionBegin;
	}

	public Date getDateOptionEnd() {
		return dateOptionEnd;
	}

	public void setDateOptionEnd(Date dateOptionEnd) {
		this.dateOptionEnd = dateOptionEnd;
	}

	public float getNbShares() {
		return nbShares;
	}

	public void setNbShares(float nbShares) {
		this.nbShares = nbShares;
	}

	public List<StockOption> getStockOptions() {
		return stockOptions;
	}

	public void setStockOptions(List<StockOption> stockOptions) {
		this.stockOptions = stockOptions;
	}

	public int getStockOptionIdToBeUpdated() {
		return stockOptionIdToBeUpdated;
	}

	public void setStockOptionIdToBeUpdated(int stockOptionIdToBeUpdated) {
		this.stockOptionIdToBeUpdated = stockOptionIdToBeUpdated;
	}

	public StockOptionServiceRemote getStockOptionService() {
		return stockOptionService;
	}

	public void setStockOptionService(StockOptionServiceRemote stockOptionService) {
		this.stockOptionService = stockOptionService;
	}

	public StockOptionBean getStockOptionBean() {
		return stockOptionBean;
	}

	public void setStockOptionBean(StockOptionBean stockOptionBean) {
		this.stockOptionBean = stockOptionBean;
	}

}
