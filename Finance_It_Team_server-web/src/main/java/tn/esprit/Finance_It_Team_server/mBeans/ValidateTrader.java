package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.Finance_It_Team_server.entities.LoginLogs;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.ServiceUser;

@ManagedBean
@SessionScoped
public class ValidateTrader {
	private String prenom;
	private String username;
	private String nom;
	private String state;
	private String email;
	private String password;

	private List<Trader> listTrader;
	private List<LoginLogs> listLogs;

	
	private int idTrader; 
	@EJB
	ServiceUser serviceUser;
	
	public void userParams(int id) {
		User user =serviceUser.getUser(id);
		prenom=user.getLastName();
		nom=user.getFirstName();
		username=user.getUserName();
		email=user.getEmail();
	}		
	public void modifyUser(int id) {
		User user =serviceUser.getUser(id);		
		user.setLastName(prenom);
		user.setFirstName(nom);
		user.setUserName(username);
		user.setEmail(email);
		serviceUser.modifyUser2(user, id);
	}		
	public void doValidateTrader(Trader trader) {
		trader.setStatusTrader("Accepted");
		serviceUser.modifyUser(trader, trader.getId());
		
	}	
	public void doBanTrader(Trader trader) {
		trader.setStatusTrader("Banned");
		serviceUser.modifyUser(trader, trader.getId());
		
	}
	public String doLogs(Trader trader) {
		idTrader=trader.getId();
		return "/PagesAdmin/logs?faces-redirect=true";
		
	}


	
	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public List<Trader> getListTrader() {
		return 		serviceUser.getAllTraders();

	}


	public void setListTrader(List<Trader> listTrader) {
		this.listTrader = listTrader;
	}


	public ServiceUser getServiceUser() {
		return serviceUser;
	}


	public void setServiceUser(ServiceUser serviceUser) {
		this.serviceUser = serviceUser;
	}
	public List<LoginLogs> getListLogs() {
		return serviceUser.listLoginLogsOfTrader(idTrader);
	}
	public void setListLogs(List<LoginLogs> listLogs) {
		this.listLogs = listLogs;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
