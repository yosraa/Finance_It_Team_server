package tn.esprit.Finance_It_Team_server.mBeans;

import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;
import tn.esprit.Finance_It_Team_server.services.Email;
import tn.esprit.Finance_It_Team_server.services.SendMail;
import tn.esprit.Finance_It_Team_server.mBeans.SystemLog;

@ViewScoped
//@SessionScoped
//@ApplicationScoped
@ManagedBean
public class WatchlistStockOptionBean {

	private float strikePriceStock;
	private float currentPriceStock;
	private TypeOptionStock typeOptionStock;  //enum hedhi lezemha classe ennum  
	private Date expirationDateStock;    // nzid nchouf le type mta3 l date
	private float volatilityStock;
	private float riskStock;
	private float timeStock;
	private StateOptionStock stateOptionStock; //enum hedhi lezemha classe ennum
	private Date dateOptionBegin;
	private Date dateOptionEnd;
	private float nbShares; 
	private List<StockOption> stockOptions;
	private Integer stockOptionIdToBeUpdated;
	public static int idStock;
	private String value;
	private   static final String ACTIONBUY = "Buy & Sell:";

	@EJB
	StockOptionServiceRemote stockOptionService;
	
	@PostConstruct //elle s'execute juste apres (l instantiation du bean et l injection de dependance(le responsable aliha @EJB w le composant logiciel qui le fait seveur d application "ejb container"))
	public void init(){
		expirationDateStock= new Date();
		dateOptionBegin= new Date();
		dateOptionEnd= new Date();
		stockOptions= stockOptionService.getAllStockOption();
	}

	public float getStrikePriceStock() {
		return strikePriceStock;
	}

	public void setStrikePriceStock(float strikePriceStock) {
		this.strikePriceStock = strikePriceStock;
	}

	public float getCurrentPriceStock() {
		return currentPriceStock;
	}

	public void setCurrentPriceStock(float currentPriceStock) {
		this.currentPriceStock = currentPriceStock;
	}

	public TypeOptionStock getTypeOptionStock() {
		return typeOptionStock;
	}

	public void setTypeOptionStock(TypeOptionStock typeOptionStock) {
		this.typeOptionStock = typeOptionStock;
	}

	public Date getExpirationDateStock() {
		return expirationDateStock;
	}

	public void setExpirationDateStock(Date expirationDateStock) {
		this.expirationDateStock = expirationDateStock;
	}

	public float getVolatilityStock() {
		return volatilityStock;
	}

	public void setVolatilityStock(float volatilityStock) {
		this.volatilityStock = volatilityStock;
	}

	public float getRiskStock() {
		return riskStock;
	}

	public void setRiskStock(float riskStock) {
		this.riskStock = riskStock;
	}

	public float getTimeStock() {
		return timeStock;
	}

	public void setTimeStock(float timeStock) {
		this.timeStock = timeStock;
	}

	public StateOptionStock getStateOptionStock() {
		return stateOptionStock;
	}

	public void setStateOptionStock(StateOptionStock stateOptionStock) {
		this.stateOptionStock = stateOptionStock;
	}

	public Date getDateOptionBegin() {
		return dateOptionBegin;
	}

	public void setDateOptionBegin(Date dateOptionBegin) {
		this.dateOptionBegin = dateOptionBegin;
	}

	public Date getDateOptionEnd() {
		return dateOptionEnd;
	}

	public void setDateOptionEnd(Date dateOptionEnd) {
		this.dateOptionEnd = dateOptionEnd;
	}

	public float getNbShares() {
		return nbShares;
	}

	public void setNbShares(float nbShares) {
		this.nbShares = nbShares;
	}

	public List<StockOption> getStockOptions() {
		return stockOptions;
	}

	public void setStockOptions(List<StockOption> stockOptions) {
		this.stockOptions = stockOptions;
	}

	public Integer getStockOptionIdToBeUpdated() {
		return stockOptionIdToBeUpdated;
	}

	public void setStockOptionIdToBeUpdated(Integer stockOptionIdToBeUpdated) {
		this.stockOptionIdToBeUpdated = stockOptionIdToBeUpdated;
	}

	public static int getIdStock() {
		return idStock;
	}

	public static void setIdStock(int idStock) {
		WatchlistStockOptionBean.idStock = idStock;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public StockOptionServiceRemote getStockOptionService() {
		return stockOptionService;
	}

	public void setStockOptionService(StockOptionServiceRemote stockOptionService) {
		this.stockOptionService = stockOptionService;
	}
	public void sendBuy(){
		
		 String destination ="myriammalek.jemmali@esprit.tn";
	        String sujet="INTELLIXX";
	        String msg="hello";
	        String login="investinvest02@gmail.com";
	        String m2p="investinvest0";
	        SendMail.send(destination, sujet, msg, login, m2p);

			SystemLog.log(ACTIONBUY + "a Stock Option");

	        
	}


	
}
